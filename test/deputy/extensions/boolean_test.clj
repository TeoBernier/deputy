(ns deputy.extensions.boolean-test
  (:require
   [clojure.test :refer :all]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer [parse-core]]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t]
   [deputy.extensions.boolean :as b :refer :all]))

(deftest test-parsing
  (testing "basic if parsing"
    (is (= (parse-if-impl '(if [true :as x :return :bool] true false))
           '[:ok true x :bool true false])))


  (testing "more advanced parsing"
    (is (= (s/Π [x :bool] (if [x :as z :return (x z)] x :bool))
           '{:node :pi, :name x
             :domain :bool
             :codomain {:node :if
                        :condition {:node :bound-var, :name x, :level 0}
                        :bind z
                        :return {:node :application
                                 :rator {:node :bound-var, :name x, :level 1}
                                 :rand {:node :bound-var, :name z, :level 0}}
                        :then {:node :bound-var, :name x, :level 0}
                        :else :bool}}))

    (is (= (s/Π [x :bool] (if [x :as z :return (x z)] x :bool))
           (s/Π [x :bool] (b/if [x :as z :return (x z)] x :bool))))

    (is (= (s/Π [x :bool] (if [y :as x :return :type] x :bool))
           '{:node :pi, :name x
             :domain :bool
             :codomain {:node :if
                        :condition {:node :free-var, :name y}
                        :bind x
                        :return :type
                        :then {:node :bound-var, :name x, :level 0}
                        :else :bool}}))))


(deftest test-tools
  (testing "unparse"

    (is (= (a/unparse (b/if [x :as x :return y] u v))
           '(if [x :as x :return y] u v))))

  (testing "freevars"
    (is (= (a/freevars (b/if [x :as x :return y] u v))
           '#{x u y v}))))

(deftest test-evaluate
  (testing "elaluate"
    (is (= (a/unparse (n/evaluate (s/pi [a :bool] (if [false :as x :return :type] :unit :bool))))
           '(Π [a :bool] :bool))))

  (testing "beta equivalence"
    (is (true? (n/beta-equiv true (b/if [true :as x :return :bool] true false))))

    (is (false? (n/beta-equiv (b/if [x :as a :return :bool] true false) (s/parse x))))))


(deftest test-typing
  (testing "simple type checks"
    (is (true? (t/type-check :type :bool)))
    (is (true? (t/type-check :bool true)))
    (is (true? (t/type-check :bool false)))

    (is (true? (t/type-check :type (s/=> :bool :bool))))

    (is (= (t/type-check :type (s/parse (s/=> :bool false)))
           '[:ko
             "Type Checking Error : Codomain of Pi-type is not a type."
             {:codomain false
              :cause [:ko "Type Checking Error : Term `term` does not have the type `vtype`."
                      {:term false, :vtype :type}]}])))

  (testing "more complex type checks"
    (is (true? (t/type-check {'x :bool} :bool (s/parse (if [x :as t :return :bool] true false)))))

    (is (true? (t/type-check :bool (s/parse ((s/the (s/=> :bool :bool) (s/λ [x] (if [x :as t :return :bool] true false))) true)))))

    (is (= (t/type-check :bool (s/parse ((s/the (s/=> :bool :bool) (s/λ [x] (if [x :as t :return :bool] nil false))) true)))
           '[:ko
             "Cannot synthesize type of computation term."
             {:term ((the (Π [_ :bool] :bool) (λ [x] (if [x :as t :return :bool] nil false))) true)
              :cause
              [:ko
               "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a function."
               {:operator (the (Π [_ :bool] :bool) (λ [x] (if [x :as t :return :bool] nil false)))
                :cause
                [:ko
                 "Type Synthesis Error : Annotation's type is not matching"
                 {:annot-type (Π [_ :bool] :bool)
                  :annot-term (λ [x] (if [x :as t :return :bool] nil false))
                  :cause
                  [:ko
                   "Type Checking Error : wrong type for abstraction."
                   {:term (λ [x] (if [x :as t :return :bool] nil false))
                    :type (Π [_ :bool] :bool)
                    :cause
                    [:ko
                     "Cannot synthesize type of computation term."
                     {:term (if [x :as t :return :bool] nil false)
                      :cause
                      [:ko
                       "Type Synthesis Error : Wrong type for then branch of `if`."
                       {:then-term nil
                        :then-type :bool
                        :cause
                        [:ko
                         "Type Checking Error : Term `term` does not have the type `vtype`."
                         {:term nil, :vtype :bool}]}]}]}]}]}]}]))))

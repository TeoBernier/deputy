(ns deputy.extensions.enum-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]))

(deftest test-parsing
  (testing "simple parses"
    (is (= (enum (ls (tag :a) (tag :b) (tag :c)))
           {:node :enum,
            :labels {:node :tags,
                     :label {:node :tag, :label :a},
                     :labels {:node :tags,
                              :label {:node :tag, :label :b},
                              :labels {:node :tags,
                                       :label {:node :tag, :label :c},
                                       :labels nil}}}}))

    (is (= (index 3)
           '{:node :succ, :idx {:node :succ, :idx {:node :succ, :idx zero}}}))

    (is (= (s/parse zero) 'zero))

    (is (= (succ zero) '{:node :succ, :idx zero})))

  (testing "simple unparse"
    (is (= (a/unparse (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))
           '(enum (ls :a :b :c :d :e :f))))

    (is (= (a/unparse (index 3))
           '(succ (succ (succ zero)))))

    
    )
  

  )



(deftest test-tools
  (testing "alpha equivalence"
    (is (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                       (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))))

    (is (not (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                            (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :f))))))

    (is (not (n/alpha-equiv (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e)))
                            (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))))))

(deftest type-system
  (testing "Π :labels :type ∋ enum"
    (is (true? (type-check (=> :labels :type)
                           (λ [x] (enum x))))))
  (testing "[x :label][xs :labels] ⊢ enum (cons x xs) ∋ 0"
    (is (true? (type-check {'x :label
                            'xs :labels}
                           (enum (cons-l x xs))
                           'zero))))
  (testing "[x :label][xs :labels][k (enum xs) ⊢ enum (cons-l x xs) ∋ (suc k)"
    (is (true? (type-check {'x :label
                            'xs :labels
                            'k (enum xs)}
                           (enum (cons-l x xs))
                           (succ k)))))

  (testing "[xs :labels][P (Π (enum xs) :type)] ⊢ :type ∋ (struct xs as x return (P x))"
    (is (true? (type-check {'xs :labels
                            'P  (Π  [_ (enum xs)] :type)}
                           :type
                           (struct xs as x return (P x))))))

  (testing "[xs :labels][n (enum xs)][P (Π (enum xs) :type)][res (struct xs as x return (P x))] ⊢ (P n) ∋ (switch n as x return (P x) with res)"
    (is (true? (type-check {'xs :labels
                            'n (enum xs)
                            'P (Π [_ (enum xs)] :type)
                            'res (struct xs as x return (P x))}
                           (app P n)
                           (switch n as x return (P x) with res))))))

(deftest test-type-check
  (testing ":labels ∋ (:a :b :c)"
    (is (true? (type-check :labels (cons-l (tag :a) (cons-l (tag :b) (cons-l (tag :c) nil))))))
    (is (true? (type-check :labels (ls (tag :a) (tag :b) (tag :c))))))

  (testing "Π :labels :type ∋ enum"
    (is (true? (type-check (=> :labels :type)
                           (λ [x] (enum x))))))

  (testing "enum (:a :b :c :d :e :f) ∋ 0"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 0)))))
  (testing "enum (:a :b :c :d :e :f) ∋ 3"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 3)))))
  (testing "enum (:a :b :c :d :e :f) ∋ 5"
    (is (true? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                           (index 5)))))

  (testing "enum (cons-tag :g (:a :b :c :d :e :f)) ∋ 6"
    (is (true? (type-check (enum (cons-l (tag :g) (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f))))
                           (index 6)))))

  (testing "Π [x [enum (:left :right)]] :type ∋ (λ [y] (switch y as x return :type with [(enum :left) [(enum :right) nil]]))"
    (is (true? (type-check (Π [x (enum (ls (tag :left) (tag :right)))] :type)
                            (λ [y] (switch y as _ return :type with
                                           (pair (enum (ls (tag :left)))
                                                 (enum (ls (tag :right)))
                                                 nil))))))))

(deftest negtest-type-check
  (testing "Index out of range"
    (is (ko-expr? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                              (index 6))))
    (is (ko-expr? (type-check (enum (ls (tag :a) (tag :b) (tag :c) (tag :d) (tag :e) (tag :f)))
                              (index 7))))))

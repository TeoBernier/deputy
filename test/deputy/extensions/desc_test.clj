(ns deputy.extensions.desc-test
  (:require
   [clojure.test :refer :all]
   [deputy.core :refer :all]
   [deputy.syntax :refer :all]
   [deputy.ast :as a :refer [mk-annot mk-app mk-bound mk-free mk-pair mk-pi mk-proj mk-sig]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.norm :as n]
   [deputy.typing :as t :refer [type-check]]
   [deputy.extensions.labels :as l :refer [ls tag]]
   [deputy.extensions.enum :as e :refer [succ struct switch]]
   [deputy.extensions.desc :refer :all]
   [deputy.extensions.ref :refer :all]))

(def n n/evaluate)

(deftest type-system
  (testing ":type ∋ descD"
    (is (true? (type-check :type
                           (mu (dref deputy.extensions.desc/descD-ctor))))))
  (testing "[T :type] ⊢ descD ∋ :k T"
    (is (true? (type-check {'T :type}
                           descD
                           (desc-k T)))))
  (testing "[S :type][T (Π S descD)] ⊢ descD ∋ (:sig S T)"
    (is (true? (type-check {'S :type
                            'T (=> S (dval deputy.extensions.desc/descD))}
                           descD
                           (desc-sig S T)))))
  (testing "[D1 descD][D2 descD)] ⊢ descD ∋ (:prod D1 D2)"
    (is (true? (type-check {'D1 descD
                            'D2 descD}
                           descD
                           (desc-prod D1 D2)))))
  (testing "[S :type][T (Π S descD)] ⊢ descD ∋ (:pi S T)"
    (is (true? (type-check {'S :type
                            'T (=> S (dval deputy.extensions.desc/descD))}
                           descD
                           (desc-pi S T)))))
  (testing "[xs :labels][cs (struct xs as _ return descD)] ⊢ descD ∋ (:struct xs cs)"
    (is (true? (type-check {'xs :labels
                            'cs (struct-desc xs)}
                           descD
                           (desc-sig-struct xs cs)))))

  (testing "[D descD][X :type] ⊢ :type ∋ (:interp D X)"
    (is (true? (type-check {'D descD
                            'X :type}
                           :type
                           (interp D X)))))

  (testing "[D descD] ⊢ :type ∋ (:μ D)"
    (is (true? (type-check {'D descD}
                           :type
                           (μ D)))))
  (testing "[D descD][xs (:interp D (:μ D))] ⊢ (:μ D) ∋ (:ctor xs)"
    (is (true? (type-check {'D descD
                            'xs (interp D (μ D))}
                           (μ D)
                           (ctor xs)))))
  (testing "[D descD][v (:μ D)] ⊢ (:interp D (:μ D)) ∋ (:attr v)"
    (is (true? (type-check {'D descD
                            'v (μ D)}
                           (interp D (μ D))
                           (inner v))))))

(deftest test-type-check
  (testing "descD ∋ (:k :unit)"
    (is (true? (type-check descD (desc-k :unit)))))

  (testing "descD ∋ (:sig (:k :unit) (λ [_] :x))"
    (is (true? (type-check descD (desc-sig :unit (λ [_] desc-x))))))

  (testing "descD ∋ (:prod (:k :unit) :x)"
    (is (true? (type-check descD (desc-prod (desc-k :unit) desc-x)))))

  (testing "descD ∋ (:pi (:k :unit) (λ [_] :x))"
    (is (true? (type-check descD (desc-pi :unit (λ [_] desc-x))))))

  (testing "descD ∋ (:struct (:cell :end) [:x [(:k :unit) nil]])"
    (is (true? (type-check descD (desc-sig-struct (ls (tag :cell)
                                                      (tag :end))
                                                  (pair desc-x
                                                        (desc-k :unit)
                                                        nil))))))

  (testing "(:interp (:sig :unit (λ [x] (:k :unit))) :unit) ∋ :attr (the (:mu (:sig :unit (λ [x] (:k :unit)))) (:ctor [nil nil]))"
    (is (true? (type-check (n (interp (desc-sig :unit (λ [_] (desc-k :unit))) :unit))
                           (inner (the (mu (desc-sig :unit (λ [_] (desc-k :unit)))) (ctor [nil nil])))))))

  (testing "(sig :unit :unit) ∋  (π2 (((the (pi [desc descD] (pi (:mu desc) (:interp desc (:mu desc))))
                                    (λ [desc ctor] (:attr ctor)))) (:struct (:a :b :c :d)
                                                                                   [(:sig :unit (λ [x] (:k :unit)))
                                                                                    [(:k :unit)
                                                                                     [(:k :unit)
                                                                                      [(:k :unit) nil]]]]))
                              (:ctor [:zero [nil nil]])))"
    (is (true? (type-check (sig :unit :unit)
                           (π2 (((the (Π [desc (href deputy.extensions.desc/descD)] (Π [_ (mu desc)] (interp desc (mu desc))))
                                      (λ [desc] (λ [ctor] (inner ctor))))
                                 (desc-sig-struct (ls (tag :a) (tag :b) (tag :c) (tag :d))
                                                  (pair (desc-sig :unit (λ [_] (desc-k :unit)))
                                                        (desc-k :unit)
                                                        (desc-k :unit)
                                                        (desc-k :unit)
                                                        nil)))
                                (ctor (pair zero nil nil)))))))))

(def nat-desc (desc-sig-struct (ls (tag :zero) (tag :succ)) (pair (desc-k :unit) desc-x nil)))

(def nat-type (mu (dval nat-desc)))

(deftest nat

  (testing "descD ∋ NatD"
    (is (true? (type-check descD (dval deputy.extensions.desc-test/nat-desc)))))

  (testing "(:mu NatD) ∋ ze"
    (is (true? (type-check nat-type
                           (ctor [zero nil])))))

  (testing "[n (:mu NatD)] ⊢ (:mu NatD) ∋ (su n)"
    (is (true? (type-check {'n nat-type}
                           nat-type
                           (ctor (pair (succ zero) n)))))))

(def tree-desc
  (desc-sig-struct (ls (tag :leaf) (tag :node))
                   (pair (desc-k :unit)
                         (desc-prod desc-x (desc-prod (desc-k A) desc-x))
                         nil)))

(def tree-type (mu (dval tree-desc)))


(def tree-nat-desc
  (desc-sig-struct (ls (tag :leaf) (tag :node))
                   (pair (desc-k :unit)
                         (desc-prod desc-x (desc-prod (desc-k (dval nat-type)) desc-x))
                         nil)))

(def tree-nat-type (mu (dval tree-nat-desc)))

(deftest bintree
  (testing "[A :type] ⊢ descD ∋ (treeD A)"
    (is (true? (type-check {'A :type}
                           descD
                           (dval deputy.extensions.desc-test/tree-desc)))))


  (testing "[A :type] ⊢ (:mu (treeD A)) ∋ :leaf"
    (is (true? (type-check {'A :type}
                           tree-type
                           (ctor [zero nil])))))

  (testing "[A :type] ⊢ (:mu (treeD A)) ∋ (:node [l [a r]])"
    (is (true? (type-check {'A :type
                            'tl tree-type
                            'a (dvar A)
                            'tr tree-type}
                           tree-type
                           (ctor (pair (succ zero) tl a tr))))))


  (testing "descD ∋ (treeD nat)"
    (is (true? (type-check descD (dval deputy.extensions.desc-test/tree-nat-desc)))))


  (testing "[A :type] ⊢ (:mu (treeD nat)) ∋ :leaf"
    (is (true? (type-check
                tree-nat-type
                (ctor [zero nil])))))

  (testing "[A :type] ⊢ (:mu (treeD nat)) ∋ (:node [l [nat r]])"
    (is (true? (type-check {'tl tree-nat-type
                            'a nat-type
                            'tr tree-nat-type}
                           tree-nat-type
                           (ctor (pair (succ zero) tl a tr))))))

  (testing "(:mu (treeD nat)) ∋ (:node [:leaf [0 :leaf]])"
    (is (true? (type-check tree-nat-type
                         ;;tree
                           (ctor (pair
                                  (succ zero)
                                  ;; leaf
                                  (ctor [zero nil])
                                  ;; 0
                                  (ctor [zero nil])
                                  ;; leaf
                                  (ctor [zero nil])))))))

  (testing "(:mu (treeD nat)) ∋ (:node [[:leaf [1 :leaf]] [4 leaf]])"
    (is (true? (type-check tree-nat-type
                          ;;tree
                           (ctor
                            (pair (succ zero)
                                  ;; tree
                                  (ctor
                                   (pair (succ zero)
                                         ;; leaf
                                         (ctor [zero nil])
                                         ;; 1
                                         (ctor
                                          (pair (succ zero)
                                                (ctor [zero nil])))
                                         ;; leaf
                                         (ctor [zero nil])))
                                  ;; 4
                                  (ctor
                                   (pair (succ zero)
                                         (ctor
                                          (pair (succ zero)
                                                (ctor
                                                 (pair (succ zero)
                                                       (ctor [zero nil])))))))
                                  ;; leaf
                                  (ctor [zero nil]))))))))

 ;;equivalence ? XXX: what is this testing?
 ;;  (t/type-check (n (parse (sig [e (enum (:a :b :c :d))] (case e as x return :type with [:label [:unit [:labels [:enum nil]]]]))))
 ;;                (parse (:attr (the (:mu (:struct (:a :b :c :d) [(:k :label) [(:k :unit) [(:k :labels) [(:k :enum) nil]]]])) (:ctor [(index 1) nil])))))
 ;;  => true

 ;; XXX: what is this testing???
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:k :unit)))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:sig :unit (fn [x] :x))))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:prod (:k :unit) :x)))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:pi :unit (fn [x] :x))))))
 ;; => true)
 ;; (t/type-check :type (n (parse ((the (pi descD :type) (fn [d] (:interp d (:mu d)))) (:struct (:cell :end) [:x [(:k :unit) nil]])))))
;; => true

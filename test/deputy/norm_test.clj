(ns deputy.norm-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :as s :refer [parse fun parse-core]]
   [deputy.ast :as a]
   [deputy.norm :refer :all]))

(deftest test-evaluate
  (testing "beta-reduction"
    (is (= (a/unparse :index (s/parse ((fun [x] (fun [y] (x y))) (fun [x] x))))
           '((λ (λ (#{1} #{0}))) (λ #{0}))))

    (is (= (a/unparse :index (evaluate (s/parse ((fun [x] (fun [y] (x y))) (fun [x] x)))))
           '(λ #{0}))))

  (testing "projections"
    (is (= (a/unparse (evaluate (s/parse (s/π1 [:unit nil nil]))))
           :unit)))

  )

(deftest test-beta-equiv
  (testing "lambdas"
    (is (= (beta-equiv (fun [x] x) (s/parse ((fun [x] (fun [y] (x y))) (fun [z] z))))
           true))

    (is (= (beta-equiv (fun [x] (fun [y] (x y))) (fun [y] (fun [x] (y x)))))
        true)

    (is (= (beta-equiv (fun [x] (fun [y] (x y))) (fun [y] (fun [x] (x y))))
           false))))
 

    

(ns deputy.syntax-test
  (:require
   [clojure.test :refer :all]
   [deputy.syntax :refer :all]))


(deftest test-gen
  (testing "Free (fresh) name generation"
    (is (= (gen-free-name "x" #{}) 'x))
    (is (= (gen-free-name "x" '#{x}) 'x'))
    (is (= (gen-free-name "x" '#{x x'}) 'x''))
    (is (= (gen-free-name "x" '#{x x' x''}) 'x-3))))

(deftest test-syntax
  (testing "variables"
    ;; a clojure variable is not directly a variable, so we need to parse explicitely
    (is (= (parse x) 
           '{:node :free-var, :name x}))
    (is (= (dvar x)
           (parse x)))
    
    (is (= (λ [x] x)
           (λ [x] (dvar x)))))

  (testing "lambda"
    (is (= (λ [x] y)
           '{:node :lambda, :name x,
             :body {:node :free-var, :name y}}))

    ;; parsing through `parse` is idempotent
    (is (= (parse (λ [x] y)) (λ [x] y))) 
    
    (is (= (fun [x] y) (λ [x] y)))

    (is (= (λ [x] (λ [y] x))
           '{:node :lambda, :name x,
             :body {:node :lambda, :name y,
                    :body {:node :bound-var, :name x, :level 1}}}))
    
    (is (= (λ [x] (λ [y] x))
           (fun [x] (fun [y] x))))

    (is (= (λ [x] y z) 
           '[:ko> "Wrong arity for lambda-abstraction, expecting 3." {:term (:deputy.syntax/lambda [x] y z), :arity 4}])))

  (testing "pi"
    (is (= (Π [x :type] x)
           '{:node :pi, :name x, 
             :domain :type, 
             :codomain {:node :bound-var, :name x, :level 0}}))

    (is (= (Π [x y z :type] y)
           '{:node :pi, :name x, 
             :domain :type, 
             :codomain {:node :pi, :name y, 
                        :domain :type, 
                        :codomain {:node :pi, :name z, 
                                   :domain :type, 
                                   :codomain {:node :bound-var, :name y, :level 1}}}}))

    (is (= (=> :type :type)
           '{:node :pi, :name _, :domain :type, :codomain :type}))

    (is (= (=> :type :type :type)
           '{:node :pi, :name _, 
             :domain :type, 
             :codomain {:node :pi, :name _, 
                        :domain :type, 
                        :codomain :type}})))

  (testing "annotations"
    (is (= (the :type nil)
           '{:node :annotation, :type :type, :term nil})))
    
  (testing "applications"
    (is (= (app (fun [x] x) y)
           '{:node :application, 
             :rator {:node :lambda, :name x, 
                     :body {:node :bound-var, :name x, :level 0}}, 
             :rand {:node :free-var, :name y}}))

    (is (= (parse ((fun [x] x) y)) (app (fun [x] x) y)))

    (is (= (app (fun [x y] x) z t)
           '{:node :application, 
             :rator {:node :application, 
                     :rator {:node :lambda, :name x, 
                             :body {:node :lambda, :name y, 
                                    :body {:node :bound-var, :name x, :level 1}}}, 
                     :rand {:node :free-var, :name z}}, 
             :rand {:node :free-var, :name t}}))
    (is (= (parse ((fun [x y] x) z t))
           (app (fun [x y] x) z t)))

    (is (= (parse (app (fun [x y] x) z t)) (app (fun [x y] x) z t)))
    
    (is (= (λ [_] (app (app (pair a b) (=> c d)) e)) (λ [_] (((pair a b) (=> c d)) e)))))

  
  (testing "pairs"

    (is (= (pair :type :type)
           '{:node :pair, :first :type, :second :type}))

    (is (= (parse [:type :type]) (pair :type :type)))

    (is (= (pair :type :type :type)
           '{:node :pair, 
             :first :type, 
             :second {:node :pair, 
                      :first :type, 
                      :second :type}}))

    (is (= (parse [:type :type :type])
           (pair :type :type :type))))


  (testing "sigmas"
    (is (= (sig [a A] [b a] C a)
           '{:node :sig, :name a, 
             :first {:node :free-var, :name A}, 
             :second {:node :sig, :name b, 
                      :first {:node :bound-var, :name a, :level 0}, 
                      :second {:node :sig, :name _, 
                               :first {:node :free-var, :name C}, 
                               :second {:node :bound-var, :name a, :level 2}}}}))

    (is (= (Σ [a A] [b a] C a) (sig [a A] [b a] C a)))

    (is (= (parse (Σ [a A] [b a] C a)) (sig [a A] [b a] C a)))

    (is (= (sig [a A] [b] C a)
           '[:ko "Cannot parse second element of pair type" 
             {:pair-type (:deputy.syntax/sigma [a A] [b] C a), 
              :term (:deputy.syntax/sigma [b] (:deputy.syntax/sigma C a)), 
              :cause [:ko> "Wrong binding: expecting `[var type]` form." {:term [b]}]}])))

  (testing "projections"

    (is (= (p1 [x y])
           '{:node :proj, :take :left, 
             :pair {:node :pair, 
                    :first {:node :free-var, :name x}, 
                    :second {:node :free-var, :name y}}}))

    (is (= (parse (p1 [x y])) (p1 [x y])))

    (is (= (p2 [x y])
           '{:node :proj, :take :right, 
             :pair {:node :pair, 
                    :first {:node :free-var, :name x}, 
                    :second {:node :free-var, :name y}}}))

    (is (= (π1 [x y]) (p1 [x y])))

    (is (= (π2 [x y]) (p2 [x y])))

    (is (= (p1 [x y] z)
           '[:ko> "Wrong left projection: expecting 1 argument" 
             {:term (:deputy.syntax/proj1 [x y] z)}]))

    (is (= (p2 [x y] z)
           '[:ko> "Wrong right projection: expecting 1 argument" 
             {:term (:deputy.syntax/proj2 [x y] z)}])))

  )


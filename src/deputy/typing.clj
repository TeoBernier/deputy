(ns deputy.typing
  (:require
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok>]]
   [deputy.syntax :as s]
   [deputy.norm :as n]))

(defonce type-check-dispatch-list (atom {}))

(defn gen-free-name [base ctx]
  (s/gen-free-name base (into #{} (keys ctx))))

(defn set-computation!
  [& terms]
  (loop [[term & terms] terms]
    (when term
      (let [term-node (u/node-type term)]
        (swap! type-check-dispatch-list #(assoc % term-node :computation))
        (recur terms)))))

(defn set-special!
  [keyword]
  (swap! type-check-dispatch-list #(assoc % keyword keyword)))

(defn set-correct!
  [& pairs]
  (loop [[pair & pairs] pairs]
    (when pair
      (let [[type term] pair
            type-node (u/node-type type)
            term-node (u/node-type term)]
        (swap! type-check-dispatch-list #(assoc % [type-node term-node] :correct))
        (recur pairs)))))

(defn type-check-dispatch-fn [_ type term]
  (let [type-node (u/node-type type)
        term-node (u/node-type term)]
    (get @type-check-dispatch-list term-node (get @type-check-dispatch-list [type-node term-node] [type-node term-node]))))

(defmulti type-check-impl #'type-check-dispatch-fn)

(defn type-check
  "Check that (syntactic) `term` has (normalized) type `vtype`
  in optional context `ctx`."
  ([vtype term] (type-check {} vtype term))
  ([ctx vtype term] (type-check-impl ctx vtype term)))

(defmulti type-synth-impl
  (fn [_ term] (:node term)))

(defn type-synth
  [term]
  (type-synth-impl {} term))


;; ***********************************************************************************
;; *********************************** TYPE-CHECK ************************************
;; ***********************************************************************************


(defmethod type-check-impl :default
  [ctx vtype term]
  [:ko "Type Checking Error : Term `term` does not have the type `vtype`." {:term (a/unparse term)
                                                                            :vtype (a/unparse vtype)}])

(defmethod type-check-impl :computation
  [ctx vtype term]
  (ok> (type-synth-impl ctx term) :as synth-vtype
       [:ko> "Cannot synthesize type of computation term." {:term (a/unparse term)}]
       (if (n/alpha-equiv vtype synth-vtype)
         true
         [:ko "Term `term` does not seem to be of type `vtype`." {:term (a/unparse term)
                                                                  :synth-type (a/unparse synth-vtype)
                                                                  :vtype (a/unparse vtype)}])))

(defmethod type-check-impl :correct
  [_ _ _] true)

(set-correct! [:type :type] [:type :unit] [:unit nil])

(defmethod type-check-impl [:type :pi]
  [ctx _ term]
  (ok> (:domain term) :as A
       (type-check-impl ctx :type A)
       [:ko> "Type Checking Error : Domain of Pi-type is not a type." {:domain (a/unparse A)}]

       (gen-free-name (:name term) ctx) :as name
       (a/mk-free name) :as x
       (:codomain term) :as B
       (a/subst x B) :as sub
       (type-check-impl (assoc ctx name (n/evaluate A)) :type sub)
       [:ko> "Type Checking Error : Codomain of Pi-type is not a type." {:codomain (a/unparse sub)}]))


(defmethod type-check-impl [:type :sig]
  [ctx _ term]
  (ok> (:first term) :as A
       (type-check-impl ctx :type A)
       [:ko> "Type Checking Error : Domain of Sigma-type is not a type." {:domain (a/unparse A)}]

       (gen-free-name (:name term) ctx) :as name
       (a/mk-free name) :as x
       (:second term) :as B
       (type-check-impl (assoc ctx name (n/evaluate A)) :type (a/subst x B))
       [:ko> "Type Checking Error : Codomain of Sigma-type is not a type." {:codomain (a/unparse B)}]))

(defmethod type-check-impl [:pi :lambda]
  [ctx vtype term]
  (ok> (:domain vtype) :as vA
       (:codomain vtype) :as vB
       (:body term) :as t
       (gen-free-name (:name term) ctx) :as name
       (a/mk-free name) :as x
       (assoc ctx name vA) :as new-ctx
       (type-check-impl new-ctx (n/evaluate (a/subst x vB)) (a/subst x t))
       [:ko> "Type Checking Error : wrong type for abstraction." {:term (a/unparse term)
                                                                  :type (a/unparse vtype)}]))

(defmethod type-check-impl [:sig :pair]
  [ctx vtype term]
  (ok> (:first vtype) :as vA
       (:first term) :as t1
       (:second vtype) :as vB
       (:second term) :as t2
       (type-check-impl ctx vA t1)
       [:ko> "Type Checking Error : wrong type for first component." {:term (a/unparse t1)
                                                                      :type (a/unparse vA)}]
       (n/evaluate t1) :as vT1
       (n/evaluate (a/subst vT1 vB)) :as vT2
       (type-check-impl ctx vT2 t2)
       [:ko> "Type Checking Error : wrong type for second component." {:term (a/unparse t2)
                                                                       :type (a/unparse vT2)}]))




;; ***********************************************************************************
;; *********************************** TYPE-SYNTH ************************************
;; ***********************************************************************************


(set-computation! :application :free-var :annotation :proj)


(defmethod type-synth-impl :default
  [_ term]
  [:ko "Type Synthesis Error : unsupported term." {:term (a/unparse term)}])

(defmethod type-synth-impl :free-var
  [ctx term]
  (get ctx (:name term) [:ko "Type Synthesis Error : No such variable in context" {:var (a/unparse term)}]))

(defmethod type-synth-impl :application
  [ctx term]

  (ok>
   (:rator term) :as e
   (:rand term) :as t
   (type-synth-impl ctx e) :as etype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a function." {:operator (a/unparse e)}]
   (when-not (= (:node etype) :pi)
     [:ko "Type Synthesis Error : Left hand-side (operator) of application must be a Pi-type." {:operator (a/unparse e)
                                                                         :type  (a/unparse etype)}])

   (:domain etype) :as A
   (:codomain etype) :as B
   (type-check-impl ctx A t)
   [:ko> "Type Synthesis Error : Wrong domain for operand." {:domain (a/unparse A)
                                                             :term (a/unparse t)}]

   (n/evaluate (a/subst (n/evaluate t) B))))

(defmethod type-synth-impl :annotation
  [ctx term]
  (ok> (:type term) :as T
       (:term term) :as t
       (type-check-impl ctx :type T)
       [:ko> "Type Synthesis Error : Annotation's type is not a type" {:annot-type (a/unparse T), :annot-term (a/unparse t)}]

       (n/evaluate T) :as vT
       (type-check-impl ctx vT t)
       [:ko> "Type Synthesis Error : Annotation's type is not matching" {:annot-type (a/unparse T), :annot-term (a/unparse t)}]
       vT))

(defmethod type-synth-impl :proj
  [ctx term]
  (ok> (:pair term) :as t
       (type-synth-impl ctx t) :as vT
       (when-not (= (:node vT) :sig)
         [:ko "Type Synthesis Error : Type of pair must be a Sigma-type." {:term (a/unparse t)
                                                    :type  (a/unparse vT)}])

       (:first vT) :as vA
       (:second vT) :as vB
       (case (:take term)
         :left vA
         :right (n/evaluate (a/subst (n/evaluate (a/mk-proj :left t)) vB)))))

(ns deputy.extensions.enum
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok> mk-meta]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))

;; ****************************************************************************
;; *********************************** ENUM ***********************************
;; ****************************************************************************

(defn mk-enum
  [labels]
  (mk-meta {:node :enum
            :labels labels} {:deputy true}))

(defn parse-enum
  [parse benv term]
  (ok>
   (parse benv (second term)) :as labels
   (mk-enum labels)))

(s/defterm enum ::enum)

(s/add-parse-pattern [(u/starts-with ::enum) parse-enum])

(defmethod a/unparse-impl :enum
  [mode node]
  (list 'enum (a/unparse-impl mode (:labels node))))

(defmethod a/freevars :enum
  [node]
  (a/freevars (:labels node)))

(defmethod a/locally-closed-impl? :enum
  [idx node]
  (a/locally-closed-impl? idx (:labels node)))


(defmethod a/normal-form-canonical? :enum
  [node]
  (a/normal-form-canonical? (:labels node)))

(defmethod a/subst-impl :enum
  [expr idx node]
  (mk-enum (a/subst-impl expr idx (:labels node))))

(defmethod a/bind-impl :enum
  [name idx node]
  (mk-enum (a/bind-impl name idx (:labels node))))


(defmethod n/evaluate-impl :enum
  [env node]
  (ok> (n/evaluate-impl env (:labels node)) :as labels
       (mk-enum labels)))

(defmethod n/alpha-equiv :enum
  [node1 node2]
  (n/alpha-equiv (:labels node1) (:labels node2)))

(defmethod t/type-check-impl [:type :enum]
  [ctx _ term]
  (ok>
   (t/type-check-impl ctx :labels (:labels term))
   [:ko> "Type Check Error : invalid argument for enum." {:term (a/unparse term)}]))

;; ****************************************************************************
;; *********************************** INDEX **********************************
;; ****************************************************************************

(s/defterm index ::index)
(s/defterm idx ::index)
(s/defterm succ ::succ)

(defn mk-succ
  [idx]
  (mk-meta {:node :succ
            :idx idx} {:deputy true}))

(defn parse-idx
  [_ _ term]
  (loop [n (second term)
         succ 'zero]
    (if-not (zero? n)
      (recur (dec n) (mk-succ succ))
      succ)))

(defn parse-succ
  [parse benv term]
  (ok>
   (parse benv (second term)) :as idx
   (mk-succ idx)))


(s/add-parse-pattern [(u/starts-with ::index) parse-idx]
                     [(u/is? 'zero) s/parse-const]
                     [(u/starts-with ::succ) parse-succ])


(defmethod a/unparse-impl :succ
  [mode node]
  (list 'succ (a/unparse-impl mode (:idx node))))


(defmethod a/freevars :succ
  [node]
  (a/freevars (:idx node)))

(defmethod a/locally-closed-impl? :succ
  [idx node]
  (a/locally-closed-impl? idx (:idx node)))


(defmethod a/normal-form-canonical? :succ
  [node]
  (a/normal-form-canonical? (:labels node)))

(defmethod a/subst-impl :succ
  [expr idx node]
  (mk-succ (a/subst-impl expr idx (:idx node))))

(defmethod a/bind-impl :succ
  [name idx node]
  (mk-succ (a/bind-impl name idx (:idx node))))

(defmethod n/alpha-equiv :succ
  [node1 node2]
  (n/alpha-equiv (:idx node1) (:idx node2)))

(defmethod n/evaluate-impl :succ
  [env node]
  (ok>
   (n/evaluate-impl env (:idx node)) :as idx
   (mk-succ idx)))


;; ****************************************************************************
;; *********************************** INDEX ENUM *****************************
;; ****************************************************************************



(defmethod t/type-check-impl [:enum 'zero]
  [_ vtype term]
  (if (= (:node (:labels vtype)) :tags)
    true
    [:ko "Type Checking Error : Term `term` does not have the type `vtype`."
     {:vtype (a/unparse vtype), :term (a/unparse term)}]))

(defmethod t/type-check-impl [:enum :succ]
  [ctx vtype term]
  (if (= (:node (:labels vtype)) :tags)
    (t/type-check-impl ctx (mk-enum (:labels (:labels vtype))) (:idx term))
    [:ko "Type Checking Error : Term `term` does not have the type `vtype`."
     {:vtype (a/unparse vtype), :term (a/unparse term)}]))


;; ****************************************************************************
;; *********************************** STRUCT *********************************
;; ****************************************************************************

(s/defterm struct ::struct)

(defn mk-struct
  [labels name return]
  (mk-meta {:node :struct
            :labels labels
            :name name
            :return return} {:deputy true}))

(defn parse-struct
  [parse benv term]
  (ok>
   (parse benv (second term)) :as labels
   (nth term 3) :as name
   (parse (cons name benv) (nth term 5)) :as return
   (mk-struct labels name return)))


(s/add-parse-pattern [(u/starts-with ::struct) parse-struct])


(defmethod a/unparse-impl :struct
  [mode node]
  (list 'struct (a/unparse-impl mode (:labels node)) 'as (:name node) 'return (a/unparse-impl mode (:return node))))

(defmethod a/freevars :struct
  [node]
  (set/union (a/freevars (:labels node))
             (a/freevars (:return node))))

(defmethod a/locally-closed-impl? :struct
  [idx node]
  (and (a/locally-closed-impl? idx (:labels node))
       (a/locally-closed-impl? (inc idx) (:return node))))

(defmethod a/normal-form-neutral? :struct
  [node]
  (and (a/normal-form-neutral? (:labels node))
       (a/normal-form-canonical? (:return node))))

(defmethod a/subst-impl :struct
  [expr idx node]
  (mk-struct (a/subst-impl expr idx (:labels node))
             (:name node)
             (a/subst-impl expr (inc idx) (:return node))))

(defmethod a/bind-impl :struct
  [name idx node]
  (mk-struct (a/bind-impl name idx (:idx node))
             (:name node)
             (a/bind-impl name (inc idx) (:return node))))

(defmethod n/evaluate-impl :struct
  [env node]
  ;; (struct labels B)
  (let
   [labels-norm (n/evaluate-impl env (:labels node))]
    (case (u/node-type labels-norm)
      ;; empty labels
      nil :unit

      ;; cons labels [l ls]
      :tags (let
             ;; A (normalized)
             [first-norm (n/evaluate-impl env (a/subst 'zero (:return node)))
              ;; ls (normalized)
              remaining-labels-norm (:labels labels-norm)
              ;; B [(succ x) / x] (not normalized)
              remaining-types (a/subst (mk-succ (a/mk-bound (:name node) 0)) (:return node))
              ;; (struct ls B [(succ x) / x]) (not normalized)
              remaining-struct (mk-struct remaining-labels-norm
                                          (:name node)
                                          remaining-types)]
              ;; (Σ A (struct ls B [(succ x) / x])) (normalized)
              (n/evaluate-impl env (a/mk-sig '_ first-norm remaining-struct)))
      ;; else
      (do
        (assert (a/normal-form-neutral? labels-norm))
        (let
        ;; B (normalized)
         [name (s/gen-free-name (:name node) (a/freevars (:return node)))
          return-norm (a/bind name (n/evaluate-impl (n/env-push env (a/mk-free name)) (:return node)))]
         ;; (struct labels B) (normalized)
          (mk-struct labels-norm (:name node) return-norm))))))


(defmethod n/alpha-equiv :struct
  [node1 node2]
  (and (n/alpha-equiv (:labels node1) (:labels node2))
       (n/alpha-equiv (:return node1) (:return node2))))

(defmethod t/type-synth-impl :struct
  [ctx term]
  (ok>
   (:labels term) :as t
   (t/type-check-impl ctx :labels t)
   [:ko> "Type Synthesis Error : bad labels." {:labels (a/unparse t)}]

   (s/gen-free-name (:name term) ctx) :as name
   (a/mk-free name) :as x
   (assoc ctx name (mk-enum t)) :as new-ctx

   (:return term) :as T
   (t/type-check-impl new-ctx :type (a/subst (a/mk-free name) T))
   [:ko> "Type Synthesis Error : bad motive." {:labels (a/unparse T)}]
   :type))

(t/set-computation! :struct)

;; ****************************************************************************
;; *********************************** SWITCH *********************************
;; ****************************************************************************

(s/defterm switch ::switch)

(defn mk-switch
  [idx name return pattern]
  (mk-meta {:node :switch
            :idx idx
            :name name
            :return return
            :pattern pattern} {:deputy true}))

(defn parse-switch
  [parse benv term]
  (ok>
   (parse benv (second term)) :as idx
   (nth term 3) :as name
   (parse (cons name benv) (nth term 5)) :as return
   (parse benv (nth term 7)) :as pattern
   (mk-switch idx name return pattern)))

(s/add-parse-pattern [(u/starts-with ::switch) parse-switch])


(defmethod a/unparse-impl :switch
  [mode node]
  (list 'switch (a/unparse-impl mode (:idx node)) 'as (:name node) 'return (a/unparse-impl mode (:return node)) 'with (a/unparse-impl mode (:pattern node))))



(defmethod a/freevars :switch
  [node]
  (set/union (a/freevars (:idx node))
             (a/freevars (:return node))
             (a/freevars (:pattern node))))

(defmethod a/locally-closed-impl? :switch
  [idx node]
  (and (a/locally-closed-impl? idx (:idx node))
       (a/locally-closed-impl? (inc idx) (:return node))
       (a/locally-closed-impl? idx (:pattern node))))

(defmethod a/normal-form-neutral? :switch
  [node]
  (and (a/normal-form-neutral? (:idx node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:pattern node))))

(defmethod a/subst-impl :switch
  [expr idx node]
  (mk-switch (a/subst-impl expr idx (:idx node))
           (:name node)
           (a/subst-impl expr (inc idx) (:return node))
           (a/subst-impl expr idx (:pattern node))))

(defmethod a/bind-impl :switch
  [name idx node]
  (mk-switch (a/bind-impl name idx (:idx node))
           (:name node)
           (a/bind-impl name (inc idx) (:return node))
           (a/bind-impl name idx (:pattern node))))


(defmethod n/evaluate-impl :switch
  [env node]
  ;; (switch idx B cs)
  (let
   [idx-norm (n/evaluate-impl env (:idx node))]
    (case (u/node-type idx-norm)
      ;; 0
      ;; (π1 cs) (normalized)
      'zero (n/evaluate-impl env (a/mk-proj :left (:pattern node)))

      ;; (succ n)
      :succ (let
             [name (s/gen-free-name (:name node) (a/freevars (:return node)))
              ;; B[(succ x) / x] (normalized)
              remaining-types (a/bind name (n/evaluate-impl
                                            (n/env-push env (a/mk-free name))
                                            (a/subst (mk-succ (a/mk-bound (:name node) 0)) (:return node))))
              ;; (π2 cs)
              new-cs (n/evaluate-impl env (a/mk-proj :right (:pattern node)))
              ;; n (normalized)
              n (:idx idx-norm)]
              ;; (switch n B[(succ x) / x] (π2 cs)) (normalized)
              (n/evaluate-impl env (mk-switch n (:name node) remaining-types new-cs)))
      ;; else
      (do
        (assert (a/normal-form-neutral? idx-norm))
        (let
          ;; B (normalized)
         [name (s/gen-free-name (:name node) (a/freevars (:return node)))
          return-norm (a/bind name (n/evaluate-impl (n/env-push env (a/mk-free name)) (:return node)))
          ;; cs (normalized)
          new-cs (n/evaluate-impl env (:pattern node))]
          ;; (switch idx B cs) (normalized)
          (mk-switch idx-norm (:name node) return-norm new-cs))))))


(defmethod n/alpha-equiv :switch
  [node1 node2]
  (and (n/alpha-equiv (:idx node1) (:idx node2))
       (n/alpha-equiv (:return node1) (:return node2))
       (n/alpha-equiv (:pattern node1) (:pattern node2))))

(defmethod t/type-synth-impl :switch
  [ctx term]
  (ok>
   (:idx term) :as ne
   (t/type-synth-impl ctx ne) :as netype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a switch."
    {:operator (a/unparse ne)}]

   (when-not (= (:node netype) :enum)
     [:ko "Type Synthesis Error : Left hand-side (operator) of application must be an enum."
      {:operator (a/unparse ne)
       :type (a/unparse netype)}])

   (:labels netype) :as vl

   (s/gen-free-name (:name term) ctx) :as name
   (a/mk-free name) :as x
   (assoc ctx name netype) :as new-ctx

   (:return term) :as T

   (t/type-check-impl new-ctx :type (a/subst (a/mk-free name) T))
   [:ko> "Type Synthesis Error : bad motive." {:labels (a/unparse T)}]

   (:pattern term) :as cs
   (n/evaluate (mk-struct vl (:name term) T)) :as struct
   (t/type-check-impl ctx struct cs)
   [:ko> "Type Synthesis Error : bad switchs" {:switchs (a/unparse cs)
                                             :expected (a/unparse struct)}]

   (n/evaluate (a/subst (n/evaluate ne) T))))

(t/set-computation! :switch)

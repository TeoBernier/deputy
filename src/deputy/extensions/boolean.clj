(ns deputy.extensions.boolean
  (:refer-clojure :exclude [if]) 
  (:require
   [clojure.set :as set]
   [clojure.test :refer [is]]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok> mk-meta]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))


(defn mk-if
  [cnd bind Tret tthen telse]
  (mk-meta {:node :if
            :condition cnd
            :bind bind
            :return Tret
            :then tthen
            :else telse}
           {:deputy true}))


(defn parse-if-impl [t]
  (ok> (when (not= (count t) 4)
         [:ko> "Wrong arity for if expression, expecting 4." {:term t
                                                              :arity (count t)}])
       t :as [_ u tthen telse]
       (when (or (not (vector? u))
                 (not= (count u) 5))
         [:ko> "Wrong condition vector for if expression." {:condition u}])
       u :as [cnd _ x _ Tret]
       (when (not (symbol? x))
         [:ko> "Condition binder must be a symbol." {:binder x}])
       [:ok cnd x Tret tthen telse]))


(defn parse-if
  [parse benv term]
  (ok> (parse-if-impl term) :as [_ cnd x Tret tthen telse]
       (parse benv cnd) :as cnd
       (conj benv x) :as benv'
       (parse benv' Tret) :as Tret
       (parse benv tthen) :as tthen
       (parse benv telse) :as telse
       (mk-if cnd x Tret tthen telse)))

;;; XXX : this does not work locally because `if seems parsed
;;;       as a primitive in clojure ...
(s/defterm if ::if)

(s/add-parse-pattern [true? s/parse-const]
                     [false? s/parse-const]
                     [#(= % :bool) s/parse-const]
                     ;; XXX : but this works, sort of...
                     [(u/starts-with 'if) parse-if]
                     ;; and this too...
                     [(u/starts-with ::if) parse-if])


(defmethod a/unparse-impl :if
  [mode node]
  (list 'if
        [(a/unparse-impl mode (:condition node))
         :as
         (:bind node)
         :return
         (a/unparse-impl mode (:return node))]
        (a/unparse-impl mode (:then node))
        (a/unparse-impl mode (:else node))))

(defmethod a/freevars :if
  [node]
  (set/union (a/freevars (:condition node))
             (a/freevars (:return node))
             (a/freevars (:then node))
             (a/freevars (:else node))))

(defmethod a/locally-closed-impl? :if
  [idx node]
  (and (a/locally-closed-impl? idx (:condition node))
       (a/locally-closed-impl? (inc idx) (:return node))
       (a/locally-closed-impl? idx (:then node))
       (a/locally-closed-impl? idx (:else node))))

(defmethod a/normal-form-neutral? :if [node]
  (and (a/normal-form-neutral? (:condition node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:then node))
       (a/normal-form-canonical? (:else node))))

(defmethod a/subst-impl :if [expr idx node]
  (mk-if (a/subst-impl expr idx (:condition node))
         (:bind node)
         (a/subst-impl expr (inc idx) (:return node))
         (a/subst-impl expr idx (:then node))
         (a/subst-impl expr idx (:else node))))


(defmethod a/bind-impl :if [name idx node]
  (mk-if (a/bind-impl name idx (:condition node))
         (:bind node)
         (a/bind-impl name (inc idx) (:return node))
         (a/bind-impl name idx (:then node))
         (a/bind-impl name idx (:else node))))

(defmethod n/evaluate-impl :if
  [env node]
  (let [cond-val (n/evaluate-impl env (:condition node))]
    (cond
      (true? cond-val) (n/evaluate-impl env (:then node))
      (false? cond-val) (n/evaluate-impl env (:else node))
      :else
      (mk-if cond-val (:bind node) (:return node)
             (n/evaluate-impl env (:then node))
             (n/evaluate-impl env (:else node))))))


(defmethod n/alpha-equiv :if
  [node1 node2]
  (and (n/alpha-equiv (:condition node1) (:condition node2))
       (n/alpha-equiv (:then node1) (:then node2))
       (n/alpha-equiv (:else node1) (:else node2))))


(t/set-correct! [:type :bool] [:bool true] [:bool false])

(t/set-computation! :if)

(defmethod t/type-synth-impl :if
  [ctx term]
  (ok> (:condition term) :as c

       (t/type-synth-impl ctx c) :as ctype
       [:ko> "Type Synthesis Error : Cannot synthesize type of condition of an if."
        {:condition (a/unparse c)}]

       (when-not (= ctype :bool)
         [:ko> "Type Synthesis Error : Condition of `if` must be a boolean"
          {:condition (a/unparse c)
           :type (a/unparse ctype)}])

       (:then term) :as tthen
       (:else term) :as telse

       (s/gen-free-name (:bind term) ctx) :as name
       (a/mk-free name) :as x
       (a/subst x (:return term)) :as T
       (assoc ctx name :bool) :as new-ctx

       (t/type-check-impl new-ctx :type T)
       [:ko> "Type Synthesis Error : Return of `if` is not a type." {:motive (a/unparse T)}]

       ;; old:
       ;;(n/evaluate (a/mk-lam (:bind term) (:return term))) :as vT

       ;; old:
       ;;(vT true) :as vTtrue
       ;; new:
       (n/evaluate (a/subst true (:return term))) :as vTtrue

       (t/type-check-impl ctx vTtrue tthen)
       [:ko> "Type Synthesis Error : Wrong type for then branch of `if`."
        {:then-term (a/unparse tthen)
         :then-type (a/unparse vTtrue)}]

       ;; old:
       ;; (vT false) :as vTfalse
       ;;new:
       (n/evaluate (a/subst false (:return term))) :as vTfalse

       (t/type-check-impl ctx vTfalse telse)
       [:ko> "Type Synthesis Error : Wrong type for else branch of `if`."
        {:else-term (a/unparse telse)
         :else-type (a/unparse vTfalse)}]
       (n/evaluate (a/subst c (:return term)))))









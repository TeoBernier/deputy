(ns deputy.extensions.fix
  (:require
   [clojure.set :as set]
   [clojure.test :refer [is]]
   [deputy.old.presyntax :as p]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok> mk-meta seq1?]]
   [deputy.syntax :as s :refer [Π λ fun app]]
   [deputy.norm :as n]
   [deputy.typing :as t]))

(def +examples-enabled+ true)

;; ****************************************************************************
;; *********************************** REC ***********************************
;; ****************************************************************************

(s/defterm rec ::rec)

(defn mk-rec
  [dom cod ret]
  (mk-meta {:node :rec :domain dom :codomain cod :return ret}
           {:deputy true}))

(defn parse-rec
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as ret
   (mk-rec dom cod ret)))

(s/add-parse-pattern [(u/starts-with ::rec) parse-rec])

(examples
 (s/parse (rec a b c))
 => '{:node :rec,
      :domain {:node :free-var, :name a},
      :codomain {:node :free-var, :name b},
      :return {:node :free-var, :name c}})

(defmethod a/unparse-impl :rec
  [mode node]
  (list 'rec
        (a/unparse-impl mode (:domain node))
        (a/unparse-impl mode (:codomain node))
        (a/unparse-impl mode (:return node))))

(examples
 (a/unparse (s/parse (rec a b c)))
 => '(rec a b c))

(defmethod a/freevars :rec
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:return node))))

(defmethod a/locally-closed-impl? :rec
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:return node))))

(defmethod a/normal-form-canonical? :rec
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:return node))))

(defmethod a/subst-impl :rec
  [expr idx node]
  (mk-rec (a/subst-impl expr idx (:domain node))
          (a/subst-impl expr idx (:codomain node))
          (a/subst-impl expr idx (:return node))))

(defmethod a/bind-impl :rec
  [name idx node]
  (mk-rec (a/bind-impl name idx (:domain node))
          (a/bind-impl name idx (:codomain node))
          (a/bind-impl name idx (:return node))))

(defmethod n/evaluate-impl :rec
  [env node]
  (ok>
   (n/evaluate-impl env (:domain node)) :as dom
   (n/evaluate-impl env (:codomain node)) :as cod
   (n/evaluate-impl env (:return node)) :as ret
   (mk-rec dom cod ret)))

(examples
 (n/evaluate (s/parse (rec (app (λ [x] x) a) (app (λ [x] x) b) (app (λ [x] x) x))))
 => (rec a b x))

(defmethod n/alpha-equiv :rec
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:return node1) (:return node2))))

(defmethod t/type-check-impl [:type :rec]
  [ctx vtype term]
  (ok>
   (:domain term) :as dom
   (t/type-check-impl ctx :type dom)
   (t/type-check-impl ctx (n/evaluate (a/mk-pi '_ dom :type)) (:codomain term))
   (t/type-check-impl ctx :type (:return term))))

(examples
 (t/type-check-impl {'A :type
                     'B (n/evaluate (s/parse (Π [_ A] :type)))
                     'X :type}
                    :type (s/parse (rec A B X)))
 => true)

;; ****************************************************************************
;; *********************************** RET ***********************************
;; ****************************************************************************

(s/defterm ret ::ret)

(defn mk-ret
  [v]
  (mk-meta {:node :ret :value v}
           {:deputy true}))

(defn parse-ret
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as v
   (mk-ret v)))

(s/add-parse-pattern [(u/starts-with ::ret) parse-ret])

(examples
 (s/parse (ret a))
 => '{:node :ret,
      :value {:node :free-var, :name a}})

(defmethod a/unparse-impl :ret
  [mode node]
  (list 'ret
        (a/unparse-impl mode (:value node))))

(examples
 (a/unparse (s/parse (ret a)))
 => '(ret a))

(defmethod a/freevars :ret
  [node]
  (a/freevars (:value node)))

(defmethod a/locally-closed-impl? :ret
  [idx node]
  (a/locally-closed-impl? idx (:value node)))

(defmethod a/normal-form-canonical? :ret
 [node]
  (a/normal-form-canonical? (:ret node)))

(defmethod a/subst-impl :ret
  [expr idx node]
  (mk-ret (a/subst-impl expr idx (:value node))))

(defmethod a/bind-impl :ret
  [name idx node]
  (mk-ret (a/bind-impl name idx (:value node))))

(defmethod n/evaluate-impl :ret
  [env node]
  (ok>
   (n/evaluate-impl env (:value node)) :as v
   (mk-ret v)))

(examples
 (n/evaluate (s/parse (ret (app (λ [x] x) v))))
 => (s/parse (ret v)))

(defmethod n/alpha-equiv :ret
  [node1 node2]
  (n/alpha-equiv (:value node1) (:value node2)))


(defmethod t/type-check-impl [:rec :ret]
  [ctx vtype term]
  (ok>
   (t/type-check-impl ctx (:return vtype) (:value term))
   [:ko "Type Checking Error : Term `term` does not have the type `vtype`." {:vtype (a/unparse vtype), :term (a/unparse term)}]))

(examples
 (t/type-check-impl {'A :type
                     'B (n/evaluate (s/parse (Π [_ A] :type)))
                     'X :type
                     'v (s/parse X)}
                    (s/parse (rec A B X))
                    (s/parse (ret v)))
 => true)

;; ****************************************************************************
;; *********************************** RECUR***********************************
;; ****************************************************************************

(s/defterm rec-call ::rec-call)

(defn mk-rec-call
  [as k]
  (mk-meta {:node :rec-call :args as :kont k}
           {:deputy true}))

(defn parse-rec-call
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as as
   (parse benv (nth term 2)) :as k
   (mk-rec-call as k)))

(s/add-parse-pattern [(u/starts-with ::rec-call) parse-rec-call])

(examples
 (s/parse (rec-call a b))
 => '{:node :rec-call,
      :args {:node :free-var, :name a}
      :kont {:node :free-var, :name b}})

(defmethod a/unparse-impl :rec-call
  [mode node]
  (list 'rec-call
        (a/unparse-impl mode (:args node))
        (a/unparse-impl mode (:kont node))))

(examples
 (a/unparse (s/parse (rec-call a b)))
 => '(rec-call a b))

(defmethod a/freevars :rec-call
  [node]
  (set/union (a/freevars (:args node))
             (a/freevars (:kont node))))

(defmethod a/locally-closed-impl? :rec-call
  [idx node]
  (and (a/locally-closed-impl? idx (:args node))
       (a/locally-closed-impl? idx (:kont node))))

(defmethod a/normal-form-canonical? :rec-call
  [node]
  (and (a/normal-form-canonical? (:args node))
       (a/normal-form-canonical? (:kont node))))

(defmethod a/subst-impl :rec-call
  [expr idx node]
  (mk-rec-call (a/subst-impl expr idx (:args node))
            (a/subst-impl expr idx (:kont node))))

(defmethod a/bind-impl :rec-call
  [name idx node]
  (mk-rec-call (a/bind-impl name idx (:args node))
            (a/bind-impl name idx (:kont node))))

(defmethod n/evaluate-impl :rec-call
  [env node]
  (ok>
   (n/evaluate-impl env (:args node)) :as as
   (n/evaluate-impl env (:kont node)) :as k
   (mk-rec-call as k)))

(examples
 (n/evaluate (s/parse (rec-call (app (λ [x] x) a) (app (λ [x] x) k))))
 => (s/parse (rec-call a k)))


(defmethod n/alpha-equiv :rec-call
  [node1 node2]
  (and (n/alpha-equiv (:args node1) (:args node2))
       (n/alpha-equiv (:kont node1) (:kont node2))))

(defmethod t/type-check-impl [:rec :rec-call]
  [ctx vtype term]
  (ok>
   (t/type-check-impl ctx (:domain vtype) (:args term))
   (t/type-check-impl ctx (n/evaluate (a/mk-pi '_ (a/mk-app (:codomain vtype) (:args term)) (mk-rec (:domain vtype) (:codomain vtype) (:return vtype)))) (:kont term))
   [:ko "Type Checking Error : Term `term` does not have the type `vtype`." {:vtype (a/unparse vtype), :term (a/unparse term)}]))

(examples
 (t/type-check-impl {'A :type
                     'B (n/evaluate (s/parse (Π [_ A] :type)))
                     'X :type
                     'a (s/parse A)
                     'k (n/evaluate (s/parse (Π [_ (B a)] (rec A B X))))}
                    (s/parse (rec A B X))
                    (s/parse (rec-call a k)))
 => true)

;; ****************************************************************************
;; *********************************** RED ***********************************
;; ****************************************************************************

(s/defterm red ::red)

(defn mk-red
  [dom cod ret src rec]
  (mk-meta {:node :red :domain dom :codomain cod :return ret :code src :rec rec }
           {:deputy true}))

(defn parse-red
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as ret
   (parse benv (nth term 4)) :as src
   (parse benv (nth term 5)) :as rec
   (mk-red dom cod ret src rec)))

(s/add-parse-pattern [(u/starts-with ::red) parse-red])

(examples
 (s/parse (red a b c d e))
 => '{:node :red,
      :domain {:node :free-var, :name a},
      :codomain {:node :free-var, :name b},
      :return {:node :free-var, :name c},
      :code {:node :free-var, :name d},
      :rec {:node :free-var, :name e}})

(defmethod a/unparse-impl :red
  [mode node]
  (list 'red
        (a/unparse-impl mode (:domain node))
        (a/unparse-impl mode (:codomain node))
        (a/unparse-impl mode (:return node))
        (a/unparse-impl mode (:code node))
        (a/unparse-impl mode (:rec node))))

(examples
 (a/unparse (s/parse (red a b c d e)))
 => '(red a b c d e))


(defmethod a/freevars :red
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:return node))
             (a/freevars (:code node)
             (a/freevars (:rec node)))))

(defmethod a/locally-closed-impl? :red
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:return node))
       (a/locally-closed-impl? idx (:code node))
       (a/locally-closed-impl? idx (:rec node))))

(defmethod a/normal-form-canonical? :red
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:return node))
       (a/normal-form-canonical? (:code node))
       (a/normal-form-canonical? (:rec node))))

(defmethod a/subst-impl :red
  [expr idx node]
  (mk-red (a/subst-impl expr idx (:domain node))
           (a/subst-impl expr idx (:codomain node))
           (a/subst-impl expr idx (:return node))
           (a/subst-impl expr idx (:code node))
           (a/subst-impl expr idx (:rec node))))

(defmethod a/bind-impl :red
  [name idx node]
  (mk-red (a/bind-impl name idx (:domain node))
           (a/bind-impl name idx (:codomain node))
           (a/bind-impl name idx (:return node))
           (a/bind-impl name idx (:code node))
           (a/bind-impl name idx (:rec node))))

(defmethod n/evaluate-impl :red
  [env node]
  (loop [i (n/evaluate-impl env (:code node))]
    (cond
      (= (:node i) :ret)
      (:value i)
      (= (:node i) :rec-call)
      (recur (n/evaluate-impl env (a/mk-app (:kont i) (a/mk-app (:rec node) (:args i)))))
      :else
      (mk-red (n/evaluate-impl env (:domain node))
              (n/evaluate-impl env (:codomain node))
              (n/evaluate-impl env (:return node))
              i
              (n/evaluate-impl env (:rec node))))))

(examples
 (n/evaluate (s/parse (red (app (λ [y] y) dom)
                           (app (λ [y] y) cod)
                           (app (λ [y] y) ret)
                           (app (λ [y] y) code)
                           (app (λ [y] y) rec))))
=> (s/parse (red dom cod ret code rec))

 (n/evaluate (s/parse (red (app (λ [y] y) dom)
                           (app (λ [y] y) cod)
                           (app (λ [y] y) ret)
                           (app (λ [y] y) (ret bc))
                           (app (λ [y] y) rec))))
=> (s/parse bc)

 (n/evaluate (s/parse (red (app (λ [y] y) dom)
                           (app (λ [y] y) cod)
                           (app (λ [y] y) ret)
                           (app (λ [y] y) (rec-call a k))
                           (app (λ [y] y) r))))
=> (s/parse (red dom cod ret (k (r a)) r))

 (n/evaluate (s/parse (red (app (λ [y] y) dom)
                           (app (λ [y] y) cod)
                           (app (λ [y] y) ret)
                           (app (λ [y] y) (rec-call a (λ [x] (ret x))))
                           (app (λ [y] y) r))))
=> (s/parse (r a)))


(defmethod n/alpha-equiv :red
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:return node1) (:return node2))
       (n/alpha-equiv (:code node1) (:code node2))
       (n/alpha-equiv (:rec node1) (:rec node2))))

(defmethod t/type-synth-impl :red
  [ctx term]
  (ok>
   (t/type-check-impl ctx :type (:domain term))
   [:ko> "Type Synthesis Error : bad domain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (a/mk-pi '_ (:domain term) :type)) :as vpi-dom-type
   (t/type-check-impl ctx vpi-dom-type (:codomain term))
   [:ko> "Type Synthesis Error : bad codomain." {:labels (a/unparse (:domain term))}]

   (t/type-check-impl ctx :type (:return term))
   [:ko> "Type Synthesis Error : bad return type." {:labels (a/unparse (:domain term))}]

   (n/evaluate (mk-rec (:domain term) (:codomain term) (:return term))) :as vrec
   (t/type-check-impl ctx vrec (:code term))
   [:ko> "Type Synthesis Error : bad code." {:labels (a/unparse (:code term))}]

   (n/evaluate (a/mk-pi '_ (:domain term) (a/mk-app (:codomain term) (a/mk-bound '_ 0)))) :as vpi-dom-cod
   (t/type-check-impl ctx vpi-dom-cod (:rec term))
   [:ko> "Type Synthesis Error : bad recursor." {:labels (a/unparse (:rec term))}]

   (n/evaluate (:return term))))


(examples
 (t/type-synth-impl {'A :type
                     'B (n/evaluate (s/parse (Π [_ A] :type)))
                     'X :type
                     'a (s/parse (rec A B X))
                     'r (n/evaluate (s/parse (Π [a A] (B a))))}
                    (s/parse (red A B X a r)))
 => (s/parse X))

;; ****************************************************************************
;; *********************************** FIX ***********************************
;; ****************************************************************************

(s/defterm fix ::fix)

(defn mk-fix
  [dom cod init rec]
  (mk-meta {:node :fix :domain dom :codomain cod :init init :rec rec}
           {:deputy true}))

(defn parse-fix
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as dom
   (parse benv (nth term 2)) :as cod
   (parse benv (nth term 3)) :as init
   (parse benv (nth term 4)) :as rec
   (mk-fix dom cod init rec)))

(s/add-parse-pattern [(u/starts-with ::fix) parse-fix])

(examples
 (s/parse (fix a b c d))
 => '{:node :fix,
      :domain {:node :free-var, :name a},
      :codomain {:node :free-var, :name b},
      :init {:node :free-var, :name c},
      :rec {:node :free-var, :name d}})

(defmethod a/unparse-impl :fix
  [mode node]
  (list 'fix
        (a/unparse-impl mode (:domain node))
        (a/unparse-impl mode (:codomain node))
        (a/unparse-impl mode (:init node))
        (a/unparse-impl mode (:rec node))))

(examples
 (a/unparse (s/parse (fix a b c d)))
 => '(fix a b c d))


(defmethod a/freevars :fix
  [node]
  (set/union (a/freevars (:domain node))
             (a/freevars (:codomain node))
             (a/freevars (:init node))
             (a/freevars (:rec node))))

(defmethod a/locally-closed-impl? :fix
  [idx node]
  (and (a/locally-closed-impl? idx (:domain node))
       (a/locally-closed-impl? idx (:codomain node))
       (a/locally-closed-impl? idx (:init node))
       (a/locally-closed-impl? idx (:rec node))))

(defmethod a/normal-form-canonical? :fix
  [node]
  (and (a/normal-form-canonical? (:domain node))
       (a/normal-form-canonical? (:codomain node))
       (a/normal-form-canonical? (:init node))
       (a/normal-form-canonical? (:rec node))))

(defmethod a/subst-impl :fix
  [expr idx node]
  (mk-fix (a/subst-impl expr idx (:domain node))
           (a/subst-impl expr idx (:codomain node))
           (a/subst-impl expr idx (:init node))
           (a/subst-impl expr idx (:rec node))))

(defmethod a/bind-impl :fix
  [name idx node]
  (mk-fix (a/bind-impl name idx (:domain node))
           (a/bind-impl name idx (:codomain node))
           (a/bind-impl name idx (:init node))
           (a/bind-impl name idx (:rec node))))

(defmethod n/alpha-equiv :fix
  [node1 node2]
  (and (n/alpha-equiv (:domain node1) (:domain node2))
       (n/alpha-equiv (:codomain node1) (:codomain node2))
       (n/alpha-equiv (:init node1) (:init node2))
       (n/alpha-equiv (:rec node1) (:rec node2))))

(defmethod n/evaluate-impl :fix
  [env node]
  (ok>
    (n/evaluate-impl env (:init node)) :as i
    (cond
      (a/normal-form-neutral? i)
      (mk-fix (n/evaluate-impl env (:domain node))
              (n/evaluate-impl env (:codomain node))
              i
              (n/evaluate-impl env (:rec node)))
      :else
      (n/evaluate-impl env
                       (mk-red (:domain node) (:codomain node)
                               (a/mk-app (:codomain node) (:init node))
                               (a/mk-app (:rec node) (:init node))
                               (a/mk-lam 'x (mk-fix (:domain node)
                                                    (:codomain node)
                                                    (a/mk-bound 'x 0)
                                                    (:rec node))))))))

(examples
  (n/evaluate (s/parse (fix (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) init)
                            (app (λ [y] y) r))))
  => (s/parse (fix dom cod init r))

  (n/alpha-equiv
   (n/evaluate (s/parse (fix (app (λ [y] y) dom)
                             (app (λ [y] y) cod)
                             (app (λ [y] y) nil)
                             (app (λ [y] y) r))))
   (n/evaluate (s/parse (red dom cod (cod nil) (r nil) (λ [x] (fix dom cod x r))))))
  => true

  (n/evaluate (s/parse (fix (app (λ [y] y) dom)
                            (app (λ [y] y) cod)
                            (app (λ [y] y) nil)
                            (λ [y] (ret bc)))))
  => (s/parse bc)

  (n/alpha-equiv
   (n/evaluate (s/parse (fix (app (λ [y] y) dom)
                             (app (λ [y] y) cod)
                             (app (λ [y] y) nil)
                             (λ [y] (rec-call a k)))))
   (n/evaluate (s/parse (red dom cod (app cod nil) 
                             (app k (fix dom cod a (λ [y] (rec-call a k))))
                             (λ[x] (fix dom cod x (λ [y] (rec-call a k))))))))
  => true)


(defmethod t/type-synth-impl :fix
  [ctx term]
  (ok>
   (t/type-check-impl ctx :type (:domain term))
   [:ko> "Type Synthesis Error : bad domain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (a/mk-pi '_ (:domain term) :type)) :as vpi-dom-type
   (t/type-check-impl ctx vpi-dom-type (:codomain term))
   [:ko> "Type Synthesis Error : bad codomain." {:labels (a/unparse (:domain term))}]

   (n/evaluate (:domain term)) :as vdom
   (t/type-check-impl ctx vdom (:init term))
   [:ko> "Type Synthesis Error : bad initial value." {:labels (a/unparse (:init term))}]

   (n/evaluate (a/mk-pi '_ (:domain term) (mk-rec (:domain term) (:codomain term) (a/mk-app (:codomain term) (a/mk-bound '_ 0))))) :as vpi-dom-rec
   (t/type-check-impl ctx vpi-dom-rec (:rec term))
   [:ko> "Type Synthesis Error : bad recursor." {:labels (a/unparse (:rec term))}]

   (n/evaluate (a/mk-app (:codomain term) (:init term)))))


(examples
 (t/type-synth-impl {'A :type
                     'B (n/evaluate (s/parse (Π [_ A] :type)))
                     'a (s/parse A)
                     'r (n/evaluate (s/parse (Π [a A] (rec A B (app B a)))))}
                    (s/parse (fix A B a r)))
 => (s/parse (B a)))

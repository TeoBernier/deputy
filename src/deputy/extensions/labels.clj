(ns deputy.extensions.labels
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok> mk-meta]]
   [deputy.syntax :as s]
   [deputy.norm :as n]
   [deputy.typing :as t]))

(defonce +examples-enabled+ true)

;; ****************************************************************************
;; *********************************** LABEL **********************************
;; ****************************************************************************

(s/defterm tag ::label)
(s/defterm label ::label)
(s/defterm l ::label)

(defn mk-label
  [label]
  (mk-meta {:node :tag
            :label label} {:deputy true}))

(defn parse-label
  [_ _ term]
  (mk-label term))

(defn parse-reserved-label
  [_ _ term]
  (ok> (when (coll? (second term))
         [:ko> "label argument shoud not be a collection" {:term term}])
       (mk-label (second term))))

(s/add-parse-pattern [(u/starts-with ::label) parse-reserved-label]
                     [(u/is? :label) s/parse-const])

(defmethod a/unparse-impl :tag
  [_ node]
  (:label node))

;; default freevars, locally-closed, subst and bind

(defmethod a/normal-form-canonical? :tag
  [_]
  true)


(defmethod n/alpha-equiv :tag
  [node1 node2]
  (= (:label node1) (:label node2)))

(defmethod n/evaluate-impl :tag
  [_ node] node)

(t/set-correct! [:type :label] [:label :tag])

;; ****************************************************************************
;; *********************************** LABELS *********************************
;; ****************************************************************************

(s/defterm ls ::labels)
(s/defterm labels ::labels)

(s/defterm cons-l ::cons)


(s/defterm vls ::vec-labels)
(s/defterm vlabels ::vec-labels)

(defn mk-labels
  [label labels]
  (mk-meta {:node :tags
            :label label
            :labels labels} {:deputy true}))

(defn parse-labels-rec
  [parse benv term]
  (if-not (empty? term)
    (ok>
     (parse benv (first term)) :as label
     (parse-labels-rec parse benv (rest term)) :as labels
     (mk-labels label labels))
    nil))

(defn parse-labels?
  [parse benv term]
  (parse-labels-rec parse benv (rest term)))

(defn parse-cons-label
  [parse benv term]
  (ok>
   (parse benv (nth term 1)) :as label
   (parse benv (nth term 2)) :as labels
   (mk-labels label labels)))


(defn parse-vec-labels-rec
  [[first & rest]]
  (if-not (empty? rest) 
    (mk-labels (mk-label first) (parse-vec-labels-rec rest))
    (mk-labels (mk-label first) nil)))


(defn parse-vec-labels
  [_ _ term]
  (assert (every? #(or (keyword? %) (symbol? %)) (rest term)))
  (parse-vec-labels-rec (rest term)))



(s/add-parse-pattern [(u/starts-with ::labels) parse-labels?]
                     [(u/is? :labels) s/parse-const]
                     [(u/starts-with ::cons) parse-cons-label]
                     [(u/starts-with ::vec-labels) parse-vec-labels])

(defn unparse-tags
  [mode node]
  (if-not (nil? (:labels node))
    (cons (a/unparse-impl mode (:label node)) (unparse-tags mode (:labels node)))
    (list (a/unparse-impl mode (:label node)))))

(defmethod a/unparse-impl :tags
  [mode node]
  (cons 'ls (unparse-tags mode node)))


(defmethod a/freevars :tags
  [node]
  (set/union (a/freevars (:label node)) (a/freevars (:labels node))))

(defmethod a/locally-closed-impl? :tags
  [idx node]
  (and (a/locally-closed-impl? idx (:label node))
       (a/locally-closed-impl? idx (:labels node))))

(defmethod a/normal-form-canonical? :tags
  [node]
  (and (a/normal-form-canonical? (:label node))
       (a/normal-form-canonical? (:labels node))))


(defmethod a/subst-impl :tags
  [expr idx node]
  (mk-labels (a/subst-impl expr idx (:label node))
             (a/subst-impl expr idx (:labels node))))


(defmethod a/bind-impl :tags
  [name idx node]
  (mk-labels (a/bind-impl name idx (:label node))
             (a/bind-impl name idx (:labels node))))


(defmethod n/evaluate-impl :tags
  [env node]
  (ok>
   (n/evaluate-impl env (:label node)) :as label
   (n/evaluate-impl env (:labels node)) :as labels
   (mk-labels label labels)))


(defmethod n/alpha-equiv :tags
  [node1 node2]
  (and (n/alpha-equiv (:label node1) (:label node2))
       (n/alpha-equiv (:labels node1) (:labels node2))))

(t/set-correct! [:type :labels] [:labels nil])

(defmethod t/type-check-impl [:labels :tags]
  [ctx _ term]
  (ok>
   (t/type-check-impl ctx :label (:label term))
   (t/type-check-impl ctx :labels (:labels term))))

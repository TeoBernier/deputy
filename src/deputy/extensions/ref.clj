(ns deputy.extensions.ref
  "Handle reference allowing external variable access."
  (:require
   [deputy.utils :as u :refer [mk-meta]]
   [deputy.syntax :as s]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :as t]))

;; *********************************************************************************
;; *********************************** REFERENCE ***********************************
;; *********************************************************************************

(s/defterm dval ::val)

(defn parse-val
  [_ _ term]
  (let [var (resolve (second term))]    
    (assert (var? var))
    (var-get var)))

(s/add-parse-pattern [(u/starts-with ::val) parse-val])

(s/defterm href ::hidden-ref)
(s/defterm dref ::ref)

(defn mk-ref
  [var hidden]
  (mk-meta {:node :ref, :var var
            :hidden hidden}
           {:deputy true}))

(defn parse-ref
  [name hidden]
  (let [var (resolve name)]
    (assert (var? var))
    (mk-ref var hidden)))

(s/add-parse-pattern [(u/starts-with ::hidden-ref) #(parse-ref (second %3) true)]
                     [(u/starts-with ::ref) #(parse-ref (second %3) false)])

(defmethod a/unparse-impl :ref
  [mode node]
  (if (:hidden node)
    (symbol (:var node))
    (a/unparse-impl mode (var-get (:var node)))))

(defmethod a/freevars :ref
  [node]
  (a/freevars (var-get (:var node))))

(defmethod a/locally-closed-impl? :ref
  [idx node]
  (a/locally-closed-impl? idx (var-get (:var node))))

(defmethod a/normal-form-canonical? :ref
  [_]
  false)

(defmethod a/subst-impl :ref
  [expr idx node]
  (a/subst-impl expr idx (var-get (:var node))))

(defmethod a/bind-impl :ref
  [name idx node]
  (a/bind-impl name idx (var-get (:var node))))

(defmethod n/evaluate-impl :ref
  [env node]
  (n/evaluate-impl env (var-get (:var node))))

(defmethod n/alpha-equiv :ref
  [node1 node2]
  (= (:var node1) (:var node2)))

(defmethod t/type-check-impl :ref
  [ctx type node]
  (t/type-check-impl ctx type (var-get (:var node))))

(t/set-special! :ref)
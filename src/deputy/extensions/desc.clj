(ns deputy.extensions.desc
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok> mk-meta]]
   [deputy.syntax :as s :refer [λ defterm]]
   [deputy.norm :as n]
   [deputy.typing :as t]
   [deputy.extensions.labels :as l :refer [vls]]
   [deputy.extensions.enum :as e :refer [index succ enum]]))

;; (defonce +examples-enabled+ true)


;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************

(defn mk-interp
  [desc type]
  (u/mk-meta {:node :interp
              :desc desc
              :type type}  {:deputy true}))

(defn mk-mu
  [desc]
  (u/mk-meta {:node :mu
              :desc desc}  {:deputy true}))

(defn mk-ctor
  [xs]
  (u/mk-meta {:node :ctor
              :xs xs} {:deputy true}))

(defn mk-inner
  [ctor]
  (u/mk-meta {:node :inner
              :ctor ctor} {:deputy true}))

(defn mk-struct-desc
  [labels]
  (mk-meta {:node :struct-desc
            :labels labels} {:deputy true}))

(defn mk-switch-desc
  [idx pattern]
  (mk-meta {:node :switch-desc
            :idx idx
            :pattern pattern} {:deputy true}))



;; ****************************************************************************
;; *********************************** CTOR ***********************************
;; ****************************************************************************


(defterm ctor ::ctor)

(defn parse-desc-ctor
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-ctor t)))

(s/add-parse-pattern [(u/starts-with ::ctor) parse-desc-ctor])

(defmethod a/unparse-impl :ctor
  [mode node]
  (list 'ctor (a/unparse-impl mode (:xs node))))

(defmethod a/freevars :ctor
  [node]
  (a/freevars (:xs node)))

(defmethod a/locally-closed-impl? :ctor
  [idx node]
  (a/locally-closed-impl? idx (:xs node)))

(defmethod a/normal-form-canonical? :ctor
  [node]
  (a/normal-form-canonical? (:xs node)))

(defmethod a/subst-impl :ctor
  [expr idx node]
  (mk-ctor (a/subst-impl expr idx (:xs node))))

(defmethod a/bind-impl :ctor
  [expr idx node]
  (mk-ctor (a/bind-impl expr idx (:xs node))))

(defmethod n/alpha-equiv :ctor
  [node1 node2]
  (n/alpha-equiv (:xs node1) (:xs node2)))




;; ****************************************************************************
;; *********************************** INTERP *********************************
;; ****************************************************************************


(defterm interp ::interp)

(defn parse-desc-interp
  [parse benv term]
  (ok>
   (parse benv (second term)) :as desc
   (parse benv (nth term 2)) :as type
   (mk-interp desc type)))

(s/add-parse-pattern [(u/starts-with ::interp) parse-desc-interp])

(defmethod a/unparse-impl :interp
  [mode node]
  (list 'interp (a/unparse-impl mode (:desc node)) (a/unparse-impl mode (:type node))))

(defmethod a/freevars :interp
  [node]
  (set/union (a/freevars (:desc node)) (a/freevars (:type node))))

(defmethod a/locally-closed-impl? :interp
  [idx node]
  (and (a/locally-closed-impl? idx (:desc node)) (a/locally-closed-impl? idx (:type node))))

(defmethod a/normal-form-canonical? :interp
  [node]
  (and (a/normal-form-canonical? (:desc node)) (a/normal-form-canonical? (:type node))))

(defmethod a/subst-impl :interp
  [expr idx node]
  (mk-interp (a/subst-impl expr idx (:desc node)) (a/subst-impl expr idx (:type node))))

(defmethod a/bind-impl :interp
  [expr idx node]
  (mk-interp (a/bind-impl expr idx (:desc node)) (a/bind-impl expr idx (:type node))))


(defmethod n/alpha-equiv :interp
  [node1 node2]
  (and (n/alpha-equiv (:desc node1) (:desc node2))
       (n/alpha-equiv (:type node1) (:type node2))))




;; ****************************************************************************
;; *********************************** MU *************************************
;; ****************************************************************************

(defterm mu ::mu)
(defterm μ ::mu)

(defn parse-desc-mu
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-mu t)))

(s/add-parse-pattern [(u/starts-with ::mu) parse-desc-mu])

(defmethod a/unparse-impl :mu
  [mode node]
  (list 'μ (a/unparse-impl mode (:desc node))))

(defmethod a/freevars :mu
  [node]
  (a/freevars (:desc node)))

(defmethod a/locally-closed-impl? :mu
  [idx node]
  (a/locally-closed-impl? idx (:desc node)))

(defmethod a/normal-form-canonical? :mu
  [node]
  (a/normal-form-canonical? (:desc node)))

(defmethod a/subst-impl :mu
  [expr idx node]
  (mk-mu (a/subst-impl expr idx (:desc node))))

(defmethod a/bind-impl :mu
  [expr idx node]
  (mk-mu (a/bind-impl expr idx (:desc node))))

(defmethod n/alpha-equiv :mu
  [node1 node2]
  (n/alpha-equiv (:desc node1) (:desc node2)))






;; ****************************************************************************
;; *********************************** SWITCH-DESC ****************************
;; ****************************************************************************

(defterm switch-desc ::switch-desc)

(defn parse-switch-desc
  [parse benv term]
  (ok>
   (parse benv (second term)) :as idx
   (parse benv (nth term 3)) :as pattern
   (mk-switch-desc idx pattern)))

(s/add-parse-pattern [(u/starts-with ::switch-desc) parse-switch-desc])

(defmethod a/unparse-impl :switch-desc
  [mode node]
  (list 'switch-desc (a/unparse-impl mode (:idx node)) 'with (a/unparse-impl mode (:pattern node))))



(defmethod a/freevars :switch-desc
  [node]
  (set/union (a/freevars (:idx node))
             (a/freevars (:pattern node))))

(defmethod a/locally-closed-impl? :switch-desc
  [idx node]
  (and (a/locally-closed-impl? idx (:idx node))
       (a/locally-closed-impl? idx (:pattern node))))

(defmethod a/normal-form-neutral? :switch-desc
  [node]
  (and (a/normal-form-neutral? (:idx node))
       (a/normal-form-canonical? (:pattern node))))

(defmethod a/subst-impl :switch-desc
  [expr idx node]
  (mk-switch-desc (a/subst-impl expr idx (:idx node))
                  (a/subst-impl expr idx (:pattern node))))

(defmethod a/bind-impl :switch-desc
  [name idx node]
  (mk-switch-desc (a/bind-impl name idx (:idx node))
                  (a/bind-impl name idx (:pattern node))))

(defmethod n/alpha-equiv :switch-desc
  [node1 node2]
  (and (n/alpha-equiv (:idx node1) (:idx node2))
       (n/alpha-equiv (:pattern node1) (:pattern node2))))






;; ****************************************************************************
;; *********************************** STRUCT-DESC ****************************
;; ****************************************************************************

(defterm struct-desc ::struct-desc)

(defn parse-struct-desc
  [parse benv term]
  (ok>
   (parse benv (second term)) :as labels
   (mk-struct-desc labels)))

(s/add-parse-pattern [(u/starts-with ::struct-desc) parse-struct-desc])

(defmethod a/unparse-impl :struct-desc
  [mode node]
  (list 'struct-desc (a/unparse-impl mode (:labels node))))



(defmethod a/freevars :struct-desc
  [node]
  (a/freevars (:labels node)))

(defmethod a/locally-closed-impl? :struct-desc
  [idx node]
  (a/locally-closed-impl? idx (:labels node)))

(defmethod a/normal-form-neutral? :struct-desc
  [node]
  (a/normal-form-neutral? (:labels node)))

(defmethod a/subst-impl :struct-desc
  [expr idx node]
  (mk-struct-desc (a/subst-impl expr idx (:labels node))))

(defmethod a/bind-impl :struct-desc
  [name idx node]
  (mk-struct-desc (a/bind-impl name idx (:labels node))))

(defmethod n/alpha-equiv :struct-desc
  [node1 node2]
  (n/alpha-equiv (:labels node1) (:labels node2)))



;; ****************************************************************************
;; *********************************** INNER **********************************
;; ****************************************************************************

(defterm inner ::inner)

(defn parse-desc-inner
  [parse benv term]
  (ok>
   (parse benv (second term)) :as t
   (mk-inner t)))

(s/add-parse-pattern [(u/starts-with ::inner) parse-desc-inner])

(defmethod a/unparse-impl :inner
  [mode node]
  (list 'inner (a/unparse-impl mode (:ctor node))))

(defmethod a/freevars :inner
  [node]
  (a/freevars (:ctor node)))

(defmethod a/locally-closed-impl? :inner
  [idx node]
  (a/locally-closed-impl? idx (:ctor node)))

(defmethod a/normal-form-neutral? :inner
  [node]
  (a/normal-form-neutral? (:ctor node)))

(defmethod a/subst-impl :inner
  [expr idx node]
  (mk-inner (a/subst-impl expr idx (:ctor node))))

(defmethod a/bind-impl :inner
  [name idx node]
  (mk-inner (a/bind-impl name idx (:ctor node))))

(defmethod n/alpha-equiv :inner
  [node1 node2]
  (n/alpha-equiv (:ctor node1) (:ctor node2)))





;; ****************************************************************************
;; *********************************** DESC? **********************************
;; ****************************************************************************

(defn desc-k?
  [node]
  (= (:first node) (e/index 0)))

(defn desc-x?
  [node]
  (= (:first node) (e/index 1)))

(defn desc-sig?
  [node]
  (= (:first node) (e/index 2)))

(defn desc-pi?
  [node]
  (= (:first node) (e/index 3)))

(defn desc-prod?
  [node]
  (= (:first node) (e/index 4)))

(defn desc-sig-struct?
  [node]
  (= (:first node) (e/index 5)))

(defn desc-pi-struct?
  [node]
  (= (:first node) (e/index 6)))


;; ****************************************************************************
;; *********************************** EVALUATE *******************************
;; ****************************************************************************



(defmethod n/evaluate-impl :ctor
  [env node]
  ;; (ctor xs) (normalized)
  (mk-ctor (n/evaluate-impl env (:xs node))))


(defmethod n/evaluate-impl :mu
  [env node]
  ;; (mu desc) (normalized)
  (mk-mu (n/evaluate-impl env (:desc node))))

(defmethod n/evaluate-impl :inner
  [env node]
  ;; (inner ctor)
  (ok>
   ;; ctor (normalized)
   (n/evaluate-impl env (:ctor node)) :as ctor
   (case (u/node-type ctor)
     :ctor (:xs ctor)
     (do (assert (a/normal-form-neutral? ctor))
         (mk-inner ctor)))))


(defmethod n/evaluate-impl :switch-desc
  [env node]
  ;; (switch-desc idx cs)
  (let [;; idx (normalized)
        idx-norm (n/evaluate-impl env (:idx node))]
    (case (u/node-type idx-norm)
      'zero (n/evaluate-impl env (:first (:pattern node)))
      :succ (let [;; (π2 cs)
                  new-cs (n/evaluate-impl env (a/mk-proj :right (:pattern node)))
                  ;; n (normalized)
                  n (:idx idx-norm)]
              (n/evaluate-impl env (mk-switch-desc n new-cs)))

      (do (assert (a/normal-form-neutral? idx-norm))
          (let [;; cs (normalized)
                new-cs (n/evaluate-impl env (:pattern node))]
            ;; (switch-desc idx cs) (normalized)
            (mk-switch-desc idx-norm new-cs))))))

(declare descD)

(defmethod n/evaluate-impl :struct-desc
  [env node]
  ;; (struct-desc labels)
  (let [;; labels (normalized)
        labels-norm (n/evaluate-impl env (:labels node))]
    (case (u/node-type labels-norm)
      ;; empty labels
      nil :unit

      ;; cons labels [l ls]
      :tags (let [;; ls (normalized)
                  remaining-labels-norm (:labels labels-norm)
                  ;; (struct-desc ls) (not normalized)
                  remaining-struct (mk-struct-desc remaining-labels-norm)]
              ;; (Σ desc (struct-desc ls)) (normalized)
              (n/evaluate-impl env (a/mk-sig '_ descD remaining-struct)))

      (do (assert (a/normal-form-neutral? labels-norm))
          ;; (struct-desc labels) (normalized)
          (mk-struct-desc labels-norm)))))

(defmethod n/evaluate-impl :interp
  [env node]
  ;; (interp ctor type)

  (ok>
   ;; ctor (normalized)
   (n/evaluate-impl env (:desc node)) :as ctor
   ;; type (normalized)
   (n/evaluate-impl env (:type node)) :as type
   ;; (ctor xs)
   (:xs ctor) :as xs
   (case (u/node-type ctor)
     :ctor
     (let [xs (:xs ctor)]
       (case (u/node-type xs)
         :pair
         (cond
           ;; (ctor [zero type])
           (desc-k? xs) (n/evaluate-impl env (a/mk-proj :right xs))

           ;; (ctor [(succ zero) nil])
           (desc-x? xs) type

           ;; (ctor [(succ (succ zero)) type (fn [x: type] desc)])
           (desc-sig? xs) (ok>
                          ;; [sig-type (fn [x: sig-type] desc)]
                           (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                          ;; sig-type (normalized)
                           (n/evaluate-impl env (a/mk-proj :left second)) :as sig-type
                          ;; (fn [x: sig-type] desc) as D -> (interp (D p) type)
                           (mk-interp (a/mk-app (a/mk-proj :right second) (a/mk-bound 's 0)) type) :as desc
                          ;; (Σ [p sig-type] (interp (D p) type)) (normalized)
                           (n/evaluate-impl env (a/mk-sig 's sig-type desc)))

          ;; (ctor [(succ (succ (succ zero))) type (fn [x: type] desc)])
           (desc-pi? xs) (ok>
                          ;; [pi-type (fn [x: pi-type] desc)]
                          (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                          ;; pi-type (normalized)
                          (n/evaluate-impl env (a/mk-proj :left second)) :as pi-type
                          ;; (fn [x: pi-type] desc) as D -> (interp (D p) type)
                          (mk-interp (a/mk-app (a/mk-proj :right second) (a/mk-bound 'p 0)) type) :as desc
                          ;; (Π [p pi-type] (interp (D p) type)) (normalized)
                          (n/evaluate-impl env (a/mk-pi 'p pi-type desc)))

          ;; (ctor [(succ (succ (succ (succ zero)))) desc1 desc2])
           (desc-prod? xs) (ok>
                            ;; [desc1 desc2] (normalized)
                            (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                            ;; (interp desc1 type) (normalized)
                            (n/evaluate-impl env (mk-interp (a/mk-proj :left second) type)) :as desc1
                            ;; (interp desc2 type) (normalized)
                            (n/evaluate-impl env (mk-interp (a/mk-proj :right second) type)) :as desc2
                            ;; (Σ [_ (interp desc1 type)] (interp desc2 type))
                            (a/mk-sig '_ desc1 desc2))

          ;; (ctor [(succ (succ (succ (succ (succ zero))))) labels struct])
           (desc-sig-struct? xs) (ok>
                                  ;; [labels struct] (normalized)
                                  (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                                  ;; labels (normalized)
                                  (n/evaluate-impl env (a/mk-proj :left second)) :as labels
                                  ;; struct (normalized)
                                  (n/evaluate-impl env (a/mk-proj :right second)) :as struct
                                  ;; (Σ [e (enum labels)] (interp (switch-desc e struct) type))
                                  (s/sig [e (enum (clj labels))] (interp (switch-desc e with (clj struct)) (clj type))))

          ;; (ctor [(succ (succ (succ (succ (succ (succ zero)))))) labels struct])
           (desc-pi-struct? xs) (ok>
                                 ;; [labels struct] (normalized)
                                 (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                                 ;; labels (normalized)
                                 (n/evaluate-impl env (a/mk-proj :left second)) :as labels
                                 ;; struct (normalized)
                                 (n/evaluate-impl env (a/mk-proj :right second)) :as struct
                                 ;; (Π [e (enum labels)] (interp (switch-desc e struct) type))
                                 (s/pi [e (enum (clj labels))] (interp (switch-desc e with (clj struct)) (clj type))))

           ;; if (:first xs) is in neutral form
           :else  (do (assert (a/normal-form-neutral? (:first xs)))
                      ;; (interp ctor type) (normalized)
                      (mk-interp ctor type)))

         ;; if xs is in neutral form
         (do (assert (a/normal-form-neutral? xs))
                      ;; (interp ctor type) (normalized)
             (mk-interp ctor type))))

     ;; if ctor is in neutral form
     (do (assert (a/normal-form-neutral? ctor))
         ;; (interp ctor type) (normalized)
         (mk-interp ctor type)))))



;; ****************************************************************************
;; *********************************** DESC-CONSTRUCTORS **********************
;; ****************************************************************************

(defterm desc-k ::desc-k)
(defterm dk ::desc-k)

(defn parse-desc-k
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 0) (parse benv (second term)))))

(def desc-x (mk-ctor (a/mk-pair (e/index 1) nil)))
(def dx desc-x)


(defn parse-desc-x
  [_ _ _]
  desc-x)

(defterm desc-sig ::desc-sig)
(defterm dsig ::desc-sig)

(defn parse-desc-sig
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 2) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))

(defterm desc-pi ::desc-pi)
(defterm dpi ::desc-pi)

(defn parse-desc-pi
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 3) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))


(defterm desc-prod ::desc-prod)
(defterm dprod ::desc-prod)

(defn parse-desc-prod
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 4) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))

(defterm desc-sig-struct ::desc-sig-struct)
(defterm dsig-struct ::desc-sig-struct)

(defn parse-desc-sig-struct
  [parse benv term]
  (ctor [(e/index 5) (clj (parse benv (second term))) (clj (parse benv (nth term 2)))]))



(defterm desc-pi-struct ::desc-pi-struct)
(defterm dpi-struct ::desc-pi-struct)

(defn parse-desc-pi-struct
  [parse benv term]
  (ctor [(e/index 6) (clj (parse benv (second term))) (clj (parse benv (nth term 2)))]))

(s/add-parse-pattern [(u/starts-with ::desc-k) parse-desc-k]
                     [(u/is? 'desc-x 'dx) parse-desc-x]
                     [(u/starts-with ::desc-sig) parse-desc-sig]
                     [(u/starts-with ::desc-pi) parse-desc-pi]
                     [(u/starts-with ::desc-prod) parse-desc-prod]
                     [(u/starts-with ::desc-sig-struct) parse-desc-sig-struct]
                     [(u/starts-with ::desc-pi-struct) parse-desc-pi-struct])



;; ****************************************************************************
;; *********************************** DESCD **********************************
;; ****************************************************************************


(def descD-ctor (dsig-struct
                 (vls :k :x :sig :pi :prod :sig-struct :pi-struct)
                 [(dk :type)
                  (dk :unit)
                  (dsig :type (λ [t] (dpi t (λ [y] dx))))
                  (dsig :type (λ [t] (dpi t (λ [y] dx))))
                  (dprod dx dx)
                  (dsig :labels (λ [t] (dk (struct-desc t))))
                  (dsig :labels (λ [t] (dk (struct-desc t))))
                  nil]))

(a/unparse descD-ctor)

(def descD (n/evaluate (mk-mu descD-ctor)))



;; ****************************************************************************
;; *********************************** TYPE CHECK AND SYNTH *******************
;; ****************************************************************************

(defmethod t/type-synth-impl :struct-desc
  [ctx term]
  (ok>
   (:labels term) :as t
   (t/type-check-impl ctx :labels t)
   [:ko> "Type Synthesis Error : bad labels." {:labels (a/unparse t)}]

   :type))


(t/set-computation! :struct-desc)

(defmethod t/type-synth-impl :switch-desc
  [ctx term]
  (ok>
   (:idx term) :as ne
   (t/type-synth-impl ctx ne) :as netype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a case-desc."
    {:operator (a/unparse ne)}]

   (when-not (= (:node netype) :enum)
     [:ko "Left hand-side (operator) of application must be an enum."
      {:operator (a/unparse ne)
       :type (a/unparse netype)}])

   (:labels netype) :as vl

   (:pattern term) :as cs
   (n/evaluate (e/mk-struct vl '_ descD)) :as struct
   (t/type-check-impl ctx struct cs)
   [:ko> "Type Synthesis Error : bad cases" {:cases (a/unparse cs)
                                             :expected (a/unparse struct)}]

   descD))


(t/set-computation! :switch-desc)


(defmethod t/type-check-impl [:mu :ctor]
  [ctx vtype term]
  (ok> (:desc vtype) :as desc
       (:xs term) :as xs

       (n/evaluate (mk-interp desc vtype)) :as dtype
       (t/type-check-impl ctx dtype xs)
       [:ko> "Type Checking Error : Invalid arguments." {:type (a/unparse vtype)
                                                         :interp (a/unparse dtype)
                                                         :ctor (a/unparse term)}]
       true))


(defmethod t/type-synth-impl :interp
  [ctx term]
  (ok> (:desc term) :as desc
       (:type term) :as type
       (t/type-check-impl ctx descD desc)
       [:ko "Type Synthesis Error : Invalid description" {:desc (a/unparse desc)}]
       (t/type-check-impl ctx :type type)
       [:ko "Type Synthesis Error : Invalid recursive argument" {:rec (a/unparse desc)}]
       :type))

(t/set-computation! :interp)

(defmethod t/type-check-impl [:type :mu]
  [ctx vtype term]
  (ok> (:desc term) :as d
       (t/type-check-impl ctx descD d)
       [:ko> "Type Checking Error : Invalid description." {:desc (a/unparse d)}]))

(defmethod t/type-synth-impl :inner
  [ctx term]
  (ok> (t/type-synth-impl ctx (:ctor term)) :as mu
       (when-not (= (:node mu) :mu)
         [:ko "Type Synthesis Error : type of `ctor` has to be `(mu D)` where `D` is of type `:desc`."
          {:ctor (a/unparse (:ctor term))
           :type (a/unparse mu)}])
       (n/evaluate (mk-interp (:desc mu) mu))))

(t/set-computation! :inner)


(examples
 (t/type-check descD descD-ctor)
 => true)

(examples
 (t/type-check descD (desc-k :unit))
 => true

 (t/type-check descD (desc-k :type))
 => true

 (t/type-check descD (desc-prod (desc-k :type) desc-x))
 => true)

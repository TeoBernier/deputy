(ns deputy.extensions.bootstrap
  (:require
   [clojure.set :as set]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [example examples ok> mk-meta]]
   [deputy.syntax :as s :refer [λ pair => Π defterm]]
   [deputy.norm :as n]
   [deputy.typing :as t]
   [deputy.extensions.labels :as l :refer [vls]]
   [deputy.extensions.enum :as e :refer [enum succ index switch struct]]
   [deputy.extensions.ref :as r :refer [dval]]))

(defonce +examples-enabled+ true)


;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************

(defn mk-interp
  [desc type]
  (u/mk-meta {:node :interp
              :desc desc
              :type type}  {:deputy true}))

(defn mk-mu
  [desc]
  (u/mk-meta {:node :mu
              :desc desc}  {:deputy true}))

(defn mk-ctor
  [xs]
  (u/mk-meta {:node :ctor
              :xs xs} {:deputy true}))

(defn mk-desc
  [desc-type args]
  (mk-meta {:node :desc-node
            :desc desc-type
            :args args} {:deputy true}))



;; ****************************************************************************
;; *********************************** CTOR ***********************************
;; ****************************************************************************


(defterm ctor ::ctor)

(defn parse-desc-ctor
  [parse benv term]
  (let [t (parse benv (second term))]
    (mk-ctor t)))

(s/add-parse-pattern [(u/starts-with ::ctor) parse-desc-ctor])

(defmethod a/unparse-impl :ctor
  [mode node]
  (list 'ctor (a/unparse-impl mode (:xs node))))

(defmethod a/freevars :ctor
  [node]
  (a/freevars (:xs node)))

(defmethod a/locally-closed-impl? :ctor
  [idx node]
  (a/locally-closed-impl? idx (:xs node)))

(defmethod a/normal-form-canonical? :ctor
  [node]
  (a/normal-form-canonical? (:xs node)))

(defmethod a/subst-impl :ctor
  [expr idx node]
  (mk-ctor (a/subst-impl expr idx (:xs node))))

(defmethod a/bind-impl :ctor
  [expr idx node]
  (mk-ctor (a/bind-impl expr idx (:xs node))))

(defmethod n/alpha-equiv :ctor
  [node1 node2]
  (n/alpha-equiv (:xs node1) (:xs node2)))



;; ****************************************************************************
;; *********************************** INTERP *********************************
;; ****************************************************************************


(defterm interp ::interp)

(defn parse-desc-interp
  [parse benv term]
  (let [desc (parse benv (second term))
        type (parse benv (nth term 2))]
    (mk-interp desc type)))

(s/add-parse-pattern [(u/starts-with ::interp) parse-desc-interp])

(defmethod a/unparse-impl :interp
  [mode node]
  (list 'interp (a/unparse-impl mode (:desc node)) (a/unparse-impl mode (:type node))))

(defmethod a/freevars :interp
  [node]
  (set/union (a/freevars (:desc node)) (a/freevars (:type node))))

(defmethod a/locally-closed-impl? :interp
  [idx node]
  (and (a/locally-closed-impl? idx (:desc node)) (a/locally-closed-impl? idx (:type node))))

(defmethod a/normal-form-canonical? :interp
  [node]
  (and (a/normal-form-canonical? (:desc node)) (a/normal-form-canonical? (:type node))))

(defmethod a/subst-impl :interp
  [expr idx node]
  (mk-interp (a/subst-impl expr idx (:desc node)) (a/subst-impl expr idx (:type node))))

(defmethod a/bind-impl :interp
  [expr idx node]
  (mk-interp (a/bind-impl expr idx (:desc node)) (a/bind-impl expr idx (:type node))))


(defmethod n/alpha-equiv :interp
  [node1 node2]
  (and (n/alpha-equiv (:desc node1) (:desc node2))
       (n/alpha-equiv (:type node1) (:type node2))))



;; ****************************************************************************
;; *********************************** MU *************************************
;; ****************************************************************************

(defterm mu ::mu)
(defterm μ ::mu)

(defn parse-desc-mu
  [parse benv term]
  (let [t (parse benv (second term))]
    (mk-mu t)))

(s/add-parse-pattern [(u/starts-with ::mu) parse-desc-mu])

(defmethod a/unparse-impl :mu
  [mode node]
  (list 'μ (a/unparse-impl mode (:desc node))))

(defmethod a/freevars :mu
  [node]
  (a/freevars (:desc node)))

(defmethod a/locally-closed-impl? :mu
  [idx node]
  (a/locally-closed-impl? idx (:desc node)))

(defmethod a/normal-form-canonical? :mu
  [node]
  (a/normal-form-canonical? (:desc node)))

(defmethod a/subst-impl :mu
  [expr idx node]
  (mk-mu (a/subst-impl expr idx (:desc node))))

(defmethod a/bind-impl :mu
  [expr idx node]
  (mk-mu (a/bind-impl expr idx (:desc node))))

(defmethod n/alpha-equiv :mu
  [node1 node2]
  (n/alpha-equiv (:desc node1) (:desc node2)))



;; ****************************************************************************
;; *********************************** DESC ***********************************
;; ****************************************************************************


(defterm dk ::desc-k)
(defterm dsig ::desc-sig)
(defterm dpi ::desc-pi)
(defterm dprod ::desc-prod)
(defterm dstruct ::desc-struct)

(def dx (mk-desc :x [nil]))

(defn mk-desc-parse
  [desc-type]
  (fn [parse benv term] (mk-desc desc-type (map (partial parse benv) (rest term)))))

(defn parse-desc-struct
  [parse benv term]
  (let [l (parse benv (nth term 1))
        ds (parse benv (nth term 2))
        e (s/gen-free-name 'e (a/freevars ds))]
    (mk-desc :sig (list (e/mk-enum l) (a/mk-lam e (e/mk-switch (a/mk-bound e 0) '_ :type ds))))))

(s/add-parse-pattern [(u/starts-with ::desc-k) (mk-desc-parse :k)]
                     [(u/is? 'dx) (fn [_ _ _] dx)]
                     [(u/starts-with ::desc-sig) (mk-desc-parse :sig)]
                     [(u/starts-with ::desc-pi) (mk-desc-parse :pi)]
                     [(u/starts-with ::desc-prod) (mk-desc-parse :prod)]
                     [(u/starts-with ::desc-struct) parse-desc-struct])

(defmethod a/unparse-impl :desc-node
  [mode node]
  (cons (symbol (str \d (symbol (:desc node)))) (map (partial a/unparse-impl mode) (:args node))))

(defmethod a/freevars :desc-node
  [node]
  (apply set/union (map a/freevars (:args node))))

(defmethod a/locally-closed-impl? :desc-node
  [idx node]
  (every? identity (map (partial a/locally-closed-impl? idx) (:args node))))

(defmethod a/normal-form-canonical? :desc-node
  [node]
  (every? identity (map a/normal-form-canonical? (:args node))))

(defmethod a/subst-impl :desc-node
  [expr idx node]
  (mk-desc (:desc node) (map (partial a/subst-impl expr idx) (:args node))))

(defmethod a/bind-impl :desc-node
  [expr idx node]
  (mk-desc (:desc node) (map (partial a/bind-impl expr idx) (:args node))))

(defmethod n/alpha-equiv :desc-node
  [node1 node2]
  (and (n/alpha-equiv (:desc node1) (:desc node2))
       (every? identity (map n/alpha-equiv (:args node1) (:args node2)))))


;; ****************************************************************************
;; *********************************** EVALUATE *******************************
;; ****************************************************************************



(defmethod n/evaluate-impl :ctor
  [env node]
  ;; (ctor xs) (normalized)
  (mk-ctor (n/evaluate-impl env (:xs node))))


(defmethod n/evaluate-impl :mu
  [env node]
  ;; (mu desc) (normalized)
  (mk-mu (n/evaluate-impl env (:desc node))))


(defmethod n/evaluate-impl :desc-node
  [env node]
  ;; (desc-type args) (normalized)
  (mk-desc (:desc node) (map (partial n/evaluate-impl env) (:args node))))


(defmethod n/evaluate-impl :interp
  [env node]
  ;; (interp desc-node type)

  (let [;; desc-node (normalized)
        desc (n/evaluate-impl env (:desc node))
        ;; type (normalized)
        type (n/evaluate-impl env (:type node))]
    (case (u/node-type desc)
      :desc-node
      (case (:desc desc)
        ;; (dk type)
        :k (n/evaluate-impl env (first (:args desc)))

        ;; (dx)
        :x type

        ;; (dsig type (fn [x: type] desc))
        :sig (let [;; sig-type (normalized)
                   sig-type (n/evaluate-impl env (first (:args desc)))
                   ;; (fn [x: sig-type] desc) as D -> (interp (D p) type)
                   desc (mk-interp (a/mk-app (second (:args desc)) (a/mk-bound 's 0)) type)]
               ;; (Σ [p sig-type] (interp (D p) type)) (normalized)
               (n/evaluate-impl env (a/mk-sig 's sig-type desc)))

          ;; (dpi type (fn [x: type] desc))
        :pi  (let [;; pi-type (normalized)
                   pi-type (n/evaluate-impl env (first (:args desc)))
                   ;; (fn [x: pi-type] desc) as D -> (interp (D p) type)
                   desc (mk-interp (a/mk-app (second (:args desc)) (a/mk-bound 'p 0)) type)]
               ;; (Π [p pi-type] (interp (D p) type)) (normalized)
               (n/evaluate-impl env (a/mk-pi 'p pi-type desc)))

        ;; (dprod desc1 desc2)
        :prod (let [;; (interp desc1 type) (normalized)
                    desc1 (n/evaluate-impl env (mk-interp (first (:args desc)) type))
                    ;; (interp desc2 type) (normalized)
                    desc2 (n/evaluate-impl env (mk-interp (second (:args desc)) type))]
                ;; (Σ [_ (interp desc1 type)] (interp desc2 type))
                (a/mk-sig '_ desc1 desc2)))

     ;; if desc is in neutral form
      (do (assert (a/normal-form-neutral? desc))
         ;; (interp ctor type) (normalized)
          (mk-interp desc type)))))



;; ****************************************************************************
;; *********************************** TYPE CHECK AND SYNTH *******************
;; ****************************************************************************


(t/set-correct! [:type :desc])


(defmethod t/type-check-impl [:desc :desc-node]
  [ctx vtype term]
  (let [args (:args term)]
    (case (:desc term)
      :k (t/type-check-impl ctx :type (first args))
      :x (t/type-check-impl ctx :unit (first args))
      :sig (ok> (t/type-check-impl ctx :type (first args))
                (t/type-check-impl ctx (n/evaluate (a/mk-pi '_ (first args) :desc)) (second args)))
      :pi (ok> (t/type-check-impl ctx :type (first args))
               (t/type-check-impl ctx (n/evaluate (a/mk-pi '_ (first args) :desc)) (second args)))
      :prod (ok> (t/type-check-impl ctx :desc (first args))
                 (t/type-check-impl ctx :desc (second args))))))


(t/set-computation! :switch-desc)


(defmethod t/type-check-impl [:mu :ctor]
  [ctx vtype term]
  (ok> (:desc vtype) :as desc
       (:xs term) :as xs

       (n/evaluate (mk-interp desc vtype)) :as dtype
       (t/type-check-impl ctx dtype xs)
       [:ko> "Type Checking Error : Invalid arguments." {:type (a/unparse vtype)
                                                         :interp (a/unparse dtype)
                                                         :ctor (a/unparse term)}]
       true))


(defmethod t/type-synth-impl :interp
  [ctx term]
  (ok> (:desc term) :as desc
       (:type term) :as type
       (t/type-check-impl ctx :desc desc)
       [:ko "Type Synthesis Error : Invalid description" {:desc (a/unparse desc)}]
       (t/type-check-impl ctx :type type)
       [:ko "Type Synthesis Error : Invalid recursive argument" {:rec (a/unparse desc)}]
       :type))

(t/set-computation! :interp)

(defmethod t/type-check-impl [:type :mu]
  [ctx vtype term]
  (ok> (:desc term) :as d
       (t/type-check-impl ctx :desc d)
       [:ko> "Type Checking Error : Invalid description." {:desc (a/unparse d)}]))

(defmethod t/type-synth-impl :inner
  [ctx term]
  (ok> (t/type-synth-impl ctx (:ctor term)) :as mu
       (when-not (= (:node mu) :mu)
         [:ko "Type Synthesis Error : type of `ctor` has to be `(mu D)` where `D` is of type `:desc`."
          {:ctor (a/unparse (:ctor term))
           :type (a/unparse mu)}])
       (n/evaluate (mk-interp (:desc mu) mu))))

(t/set-computation! :inner)




(def desc-boot-ctor (dsig
                     (enum (vls :k :x :sig :pi :prod))
                     (λ [e]
                        (switch e as _ return :desc with
                                [(dk :type)
                                 (dk :unit)
                                 (dsig :type (λ [t] (dpi t (λ [y] dx))))
                                 (dsig :type (λ [t] (dpi t (λ [y] dx))))
                                 (dprod dx dx)
                                 nil]))))

(a/unparse desc-boot-ctor)

(def desc-boot (mk-mu desc-boot-ctor))

(def desc-boot-norm (n/evaluate desc-boot))


(examples
 (t/type-check :desc desc-boot-ctor)
 => true
 (t/type-check :type (mk-mu desc-boot-ctor))
 => true
 (t/type-check desc-boot-norm (ctor (pair (index 0) :type)))
 => true
 (t/type-check desc-boot-norm (ctor (pair (index 1) nil)))
 => true
 (t/type-check desc-boot-norm (ctor (pair (index 2) :type (λ [_] (ctor (pair (index 1) nil))))))
 => true)



;; ****************************************************************************
;; *********************************** DESC-CONSTRUCTORS **********************
;; ****************************************************************************

(defterm dk2 ::desc-k2)
(defterm dsig2 ::desc-sig2)
(defterm dpi2 ::desc-pi2)
(defterm dprod2 ::desc-prod2)
(defterm dstruct2 ::desc-struct2)

(defn parse-desc-k
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 0) (parse benv (second term)))))

(def dx2 (mk-ctor (a/mk-pair (e/index 1) nil)))

(defn parse-desc-x
  [_ _ _]
  dx2)

(defn parse-desc-sig
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 2) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))


;; we define desc-struct as a variant of desc-sig
;; (desc-struct labels cs)
;; -> (desc-sig (enum labels) (λ [e] (switech-desc e cs)))
(defn parse-desc-struct2
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 2) (a/mk-pair
                                   (e/mk-enum (parse benv (second term)))
                                   (a/mk-lam 'e (e/mk-switch (a/mk-bound 'e 0) '_ desc-boot (parse benv (nth term 2))))))))


(defn parse-desc-pi
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 3) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))


(defn parse-desc-prod
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 4) (a/mk-pair (parse benv (second term)) (parse benv (nth term 2))))))

(s/add-parse-pattern [(u/starts-with ::desc-k2) parse-desc-k]
                     [(u/is? 'dx2) parse-desc-x]
                     [(u/starts-with ::desc-sig2) parse-desc-sig]
                     [(u/starts-with ::desc-struct2) parse-desc-struct2]
                     [(u/starts-with ::desc-pi2) parse-desc-pi]
                     [(u/starts-with ::desc-prod2) parse-desc-prod])

(examples
 (t/type-check desc-boot-norm (dk2 :type))
 => true
 (t/type-check desc-boot-norm dx2)
 => true
 (t/type-check desc-boot-norm (dsig2 :type (λ [_] dx2)))
 => true)



(def desc-boot-ctor2 (dsig2
                      (enum (vls :k :x :sig :pi :prod))
                      (λ [e]
                         (switch e as _ return (dval desc-boot) with
                                 [(dk2 :type)
                                  (dk2 :unit)
                                  (dsig2 :type (λ [t] (dpi2 t (λ [y] dx2))))
                                  (dsig2 :type (λ [t] (dpi2 t (λ [y] dx2))))
                                  (dprod2 dx2 dx2)
                                  nil]))))

(a/unparse desc-boot-ctor2)


(examples
 (t/type-check desc-boot-norm desc-boot-ctor2)
 => true)





;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************

(defn mk-interp2
  [desc type]
  (u/mk-meta {:node :interp2
              :desc desc
              :type type}  {:deputy true}))

(defn mk-mu2
  [desc]
  (u/mk-meta {:node :mu2
              :desc desc}  {:deputy true}))

(defn mk-switch-desc
  [idx pattern]
  (mk-meta {:node :switch-desc
            :idx idx
            :pattern pattern} {:deputy true}))




;; ****************************************************************************
;; *********************************** INTERP2 *********************************
;; ****************************************************************************


(defterm interp2 ::interp2)

(defn parse-desc-interp2
  [parse benv term]
  (let [desc (parse benv (second term))
        type (parse benv (nth term 2))]
    (mk-interp2 desc type)))

(s/add-parse-pattern [(u/starts-with ::interp2) parse-desc-interp2])

(defmethod a/unparse-impl :interp2
  [mode node]
  (list 'interp2 (a/unparse-impl mode (:desc node)) (a/unparse-impl mode (:type node))))

(defmethod a/freevars :interp2
  [node]
  (set/union (a/freevars (:desc node)) (a/freevars (:type node))))

(defmethod a/locally-closed-impl? :interp2
  [idx node]
  (and (a/locally-closed-impl? idx (:desc node)) (a/locally-closed-impl? idx (:type node))))

(defmethod a/normal-form-canonical? :interp2
  [node]
  (and (a/normal-form-canonical? (:desc node)) (a/normal-form-canonical? (:type node))))

(defmethod a/subst-impl :interp2
  [expr idx node]
  (mk-interp2 (a/subst-impl expr idx (:desc node)) (a/subst-impl expr idx (:type node))))

(defmethod a/bind-impl :interp2
  [expr idx node]
  (mk-interp2 (a/bind-impl expr idx (:desc node)) (a/bind-impl expr idx (:type node))))


(defmethod n/alpha-equiv :interp2
  [node1 node2]
  (and (n/alpha-equiv (:desc node1) (:desc node2))
       (n/alpha-equiv (:type node1) (:type node2))))




;; ****************************************************************************
;; *********************************** MU2 *************************************
;; ****************************************************************************

(defterm mu2 ::mu2)
(defterm μ ::mu2)

(defn parse-desc-mu2
  [parse benv term]
  (let [t (parse benv (second term))]
    (mk-mu2 t)))

(s/add-parse-pattern [(u/starts-with ::mu2) parse-desc-mu2])

(defmethod a/unparse-impl :mu2
  [mode node]
  (list 'μ (a/unparse-impl mode (:desc node))))

(defmethod a/freevars :mu2
  [node]
  (a/freevars (:desc node)))

(defmethod a/locally-closed-impl? :mu2
  [idx node]
  (a/locally-closed-impl? idx (:desc node)))

(defmethod a/normal-form-canonical? :mu2
  [node]
  (a/normal-form-canonical? (:desc node)))

(defmethod a/subst-impl :mu2
  [expr idx node]
  (mk-mu2 (a/subst-impl expr idx (:desc node))))

(defmethod a/bind-impl :mu2
  [expr idx node]
  (mk-mu2 (a/bind-impl expr idx (:desc node))))

(defmethod n/alpha-equiv :mu2
  [node1 node2]
  (n/alpha-equiv (:desc node1) (:desc node2)))




;; ****************************************************************************
;; *********************************** SWITCH-DESC ****************************
;; ****************************************************************************

(defterm switch-desc ::switch-desc)

(defn parse-switch-desc
  [parse benv term]
  (let [idx (parse benv (second term))
        pattern (parse benv (nth term 3))]
    (mk-switch-desc idx pattern)))

(s/add-parse-pattern [(u/starts-with ::switch-desc) parse-switch-desc])

(defmethod a/unparse-impl :switch-desc
  [mode node]
  (list 'switch-desc (a/unparse-impl mode (:idx node)) 'with (a/unparse-impl mode (:pattern node))))



(defmethod a/freevars :switch-desc
  [node]
  (set/union (a/freevars (:idx node))
             (a/freevars (:pattern node))))

(defmethod a/locally-closed-impl? :switch-desc
  [idx node]
  (and (a/locally-closed-impl? idx (:idx node))
       (a/locally-closed-impl? idx (:pattern node))))

(defmethod a/normal-form-neutral? :switch-desc
  [node]
  (and (a/normal-form-neutral? (:idx node))
       (a/normal-form-canonical? (:pattern node))))

(defmethod a/subst-impl :switch-desc
  [expr idx node]
  (mk-switch-desc (a/subst-impl expr idx (:idx node))
                  (a/subst-impl expr idx (:pattern node))))

(defmethod a/bind-impl :switch-desc
  [name idx node]
  (mk-switch-desc (a/bind-impl name idx (:idx node))
                  (a/bind-impl name idx (:pattern node))))

(defmethod n/alpha-equiv :switch-desc
  [node1 node2]
  (and (n/alpha-equiv (:idx node1) (:idx node2))
       (n/alpha-equiv (:pattern node1) (:pattern node2))))




;; ****************************************************************************
;; *********************************** DESC? **********************************
;; ****************************************************************************

(defn desc-k?
  [node]
  (= (:first node) (e/index 0)))

(defn desc-x?
  [node]
  (= (:first node) (e/index 1)))

(defn desc-sig?
  [node]
  (= (:first node) (e/index 2)))

(defn desc-pi?
  [node]
  (= (:first node) (e/index 3)))

(defn desc-prod?
  [node]
  (= (:first node) (e/index 4)))




;; ****************************************************************************
;; *********************************** EVALUATE *******************************
;; ****************************************************************************


(defmethod n/evaluate-impl :mu2
  [env node]
  ;; (mu2 desc) (normalized)
  (mk-mu2 (n/evaluate-impl env (:desc node))))


(defmethod n/evaluate-impl :switch-desc
  [env node]
  ;; (switch-desc idx cs)
  (let [;; idx (normalized)
        idx-norm (n/evaluate-impl env (:idx node))]
    (case (u/node-type idx-norm)
      'zero (n/evaluate-impl env (:first (:pattern node)))
      :succ (let [;; (π2 cs)
                  new-cs (n/evaluate-impl env (a/mk-proj :right (:pattern node)))
                  ;; n (normalized)
                  n (:idx idx-norm)]
              (n/evaluate-impl env (mk-switch-desc n new-cs)))

      (do (assert (a/normal-form-neutral? idx-norm))
          (let [;; cs (normalized)
                new-cs (n/evaluate-impl env (:pattern node))]
            ;; (switch-desc idx cs) (normalized)
            (mk-switch-desc idx-norm new-cs))))))

(defmethod n/evaluate-impl :interp2
  [env node]
  ;; (interp ctor type)

  (ok>
   ;; ctor (normalized)
   (n/evaluate-impl env (:desc node)) :as ctor
   ;; type (normalized)
   (n/evaluate-impl env (:type node)) :as type
   ;; (ctor xs)
   (:xs ctor) :as xs
   (case (u/node-type ctor)
     :ctor
     (let [xs (:xs ctor)]
       (case (u/node-type xs)
         :pair
         (cond
           ;; (ctor [zero type])
           (desc-k? xs) (n/evaluate-impl env (a/mk-proj :right xs))

           ;; (ctor [(succ zero) nil])
           (desc-x? xs) type

           ;; (ctor [(succ (succ zero)) type (fn [x: type] desc)])
           (desc-sig? xs) (ok>
                          ;; [sig-type (fn [x: sig-type] desc)]
                           (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                          ;; sig-type (normalized)
                           (n/evaluate-impl env (a/mk-proj :left second)) :as sig-type
                          ;; (fn [x: sig-type] desc) as D -> (interp (D p) type)
                           (mk-interp2 (a/mk-app (a/mk-proj :right second) (a/mk-bound 's 0)) type) :as desc
                          ;; (Σ [p sig-type] (interp (D p) type)) (normalized)
                           (n/evaluate-impl env (a/mk-sig 's sig-type desc)))

          ;; (ctor [(succ (succ (succ zero))) type (fn [x: type] desc)])
           (desc-pi? xs) (ok>
                          ;; [pi-type (fn [x: pi-type] desc)]
                          (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                          ;; pi-type (normalized)
                          (n/evaluate-impl env (a/mk-proj :left second)) :as pi-type
                          ;; (fn [x: pi-type] desc) as D -> (interp (D p) type)
                          (mk-interp2 (a/mk-app (a/mk-proj :right second) (a/mk-bound 'p 0)) type) :as desc
                          ;; (Π [p pi-type] (interp (D p) type)) (normalized)
                          (n/evaluate-impl env (a/mk-pi 'p pi-type desc)))

          ;; (ctor [(succ (succ (succ (succ zero)))) desc1 desc2])
           (desc-prod? xs) (ok>
                            ;; [desc1 desc2] (normalized)
                            (n/evaluate-impl env (a/mk-proj :right xs)) :as second
                            ;; (interp desc1 type) (normalized)
                            (n/evaluate-impl env (mk-interp2 (a/mk-proj :left second) type)) :as desc1
                            ;; (interp desc2 type) (normalized)
                            (n/evaluate-impl env (mk-interp2 (a/mk-proj :right second) type)) :as desc2
                            ;; (Σ [_ (interp desc1 type)] (interp desc2 type))
                            (a/mk-sig '_ desc1 desc2))

           ;; if (:first xs) is in neutral form
           :else  (do (assert (a/normal-form-neutral? (:first xs)))
                      ;; (interp ctor type) (normalized)
                      (mk-interp2 ctor type)))

         ;; if xs is in neutral form
         (do (assert (a/normal-form-neutral? xs))
                      ;; (interp ctor type) (normalized)
             (mk-interp2 ctor type))))

     ;; if ctor is in neutral form
     (do (assert (a/normal-form-neutral? ctor))
         ;; (interp ctor type) (normalized)
         (mk-interp2 ctor type)))))


;; ****************************************************************************
;; *********************************** REDEFINE DESC **************************
;; ****************************************************************************


(def descD-ctor (dsig2
                 (enum (vls :k :x :sig :pi :prod))
                 (λ [e]
                    (switch-desc e with
                                 [(dk2 :type)
                                  (dk2 :unit)
                                  (dsig2 :type (λ [t] (dpi2 t (λ [y] dx2))))
                                  (dsig2 :type (λ [t] (dpi2 t (λ [y] dx2))))
                                  (dprod2 dx2 dx2)
                                  nil]))))

(a/unparse descD-ctor)

(def descD (n/evaluate (mk-mu2 descD-ctor)))



;; ****************************************************************************
;; *********************************** TYPE CHECK AND SYNTH *******************
;; ****************************************************************************





(defmethod t/type-synth-impl :switch-desc
  [ctx term]
  (ok>
   (:idx term) :as ne
   (t/type-synth-impl ctx ne) :as netype
   [:ko> "Type Synthesis Error : Cannot synthesize type of left hand-side (operator) of a case-desc."
    {:operator (a/unparse ne)}]

   (when-not (= (:node netype) :enum)
     [:ko "Left hand-side (operator) of application must be an enum."
      {:operator (a/unparse ne)
       :type (a/unparse netype)}])

   (:labels netype) :as vl

   (:pattern term) :as cs
   (n/evaluate (e/mk-struct vl '_ descD)) :as struct
   (t/type-check-impl ctx struct cs)
   [:ko> "Type Synthesis Error : bad cases" {:cases (a/unparse cs)
                                             :expected (a/unparse struct)}]

   descD))


(t/set-computation! :switch-desc)


(defmethod t/type-check-impl [:mu2 :ctor]
  [ctx vtype term]
  (ok> (:desc vtype) :as desc
       (:xs term) :as xs

       (n/evaluate (mk-interp2 desc vtype)) :as dtype
       (t/type-check-impl ctx dtype xs)
       [:ko> "Type Checking Error : Invalid arguments." {:type (a/unparse vtype)
                                                         :interp2 (a/unparse dtype)
                                                         :ctor (a/unparse term)}]
       true))


(defmethod t/type-synth-impl :interp2
  [ctx term]
  (ok> (:desc term) :as desc
       (:type term) :as type
       (t/type-check-impl ctx descD desc)
       [:ko "Type Synthesis Error : Invalid description" {:desc (a/unparse desc)}]
       (t/type-check-impl ctx :type type)
       [:ko "Type Synthesis Error : Invalid recursive argument" {:rec (a/unparse desc)}]
       :type))

(t/set-computation! :interp2)

(defmethod t/type-check-impl [:type :mu2]
  [ctx vtype term]
  (ok> (:desc term) :as d
       (t/type-check-impl ctx descD d)
       [:ko> "Type Checking Error : Invalid description." {:desc (a/unparse d)}]))


;; ****************************************************************************
;; *********************************** DSTRUCT3 *******************************
;; ****************************************************************************

(defterm dstruct3 ::desc-struct3)


;; we define desc-struct as a variant of desc-sig
;; (desc-struct labels cs)
;; -> (desc-sig (enum labels) (λ [e] (switech-desc e cs)))
(defn parse-desc-struct3
  [parse benv term]
  (mk-ctor (a/mk-pair (e/index 2) (a/mk-pair
                                   (e/mk-enum (parse benv (second term)))
                                   (a/mk-lam 'e (mk-switch-desc (a/mk-bound 'e 0) (parse benv (nth term 2))))))))

(s/add-parse-pattern [(u/starts-with ::desc-struct3) parse-desc-struct3])

;; ****************************************************************************
;; *********************************** TEST BOOTSTRAP *************************
;; ****************************************************************************



(examples
 (t/type-check descD descD-ctor)
 => true)

(examples
 (t/type-check :type (mu2 (dval descD-ctor)))
 => true
 (t/type-check {'T :type} descD (dk2 T))
 => true

 (t/type-check {'S :type
                'T (=> S (dval descD))}
               descD
               (dsig2 S T))
 => true

 (t/type-check {'D1 descD
                'D2 descD}
               descD
               (dprod2 D1 D2))
 => true

 (t/type-check {'S :type
                'T (=> S (dval descD))}
               descD
               (dpi2 S T))
 => true

 (t/type-check {'xs :labels
                'cs (struct xs as _ return (dval descD))}
               descD
               (dstruct3 xs cs))
 => true

 (t/type-check {'D descD
                'X :type}
               :type
               (interp2 D X))
 => true

 (t/type-check {'D descD}
               :type
               (mu2 D))
 => true

 (t/type-check {'D descD
                'xs (interp2 D (mu2 D))}
               (mu2 D)
               (ctor xs))
 => true

 (t/type-check descD (dk2 :unit))
 => true

 (t/type-check descD (dsig2 :unit (λ [_] dx2)))
 => true

 (t/type-check descD (dprod2 (dk2 :unit) dx2))
 => true

 (t/type-check descD (dpi2 :unit (λ [_] dx2)))
 => true

 (t/type-check descD (dstruct3 (vls :cell
                                    :end)
                               (pair dx2
                                     (dk2 :unit)
                                     nil)))
 => true)

(def nat-desc (dstruct3 (vls :zero :succ) (pair (dk2 :unit) dx2 nil)))
(def nat-type (mu2 (dval nat-desc)))

(examples
 (t/type-check descD (dval nat-desc))
 => true

 (t/type-check nat-type (ctor [zero nil]))
 => true

 (t/type-check {'n nat-type} nat-type (ctor (pair (succ zero) n)))
 => true)

;; ****************************************************************************
;; *********************************** NAT PARSING ****************************
;; ****************************************************************************

(defterm nat ::nat)

(defn parse-nat-rec
  [acc n]
  (if (zero? n)
    acc
    (parse-nat-rec (mk-ctor (a/mk-pair (succ zero) acc)) (dec n))))

(defn parse-nat
  [_ _ term]
  (parse-nat-rec (ctor (pair zero nil)) (second term)))

(s/add-parse-pattern [(u/starts-with ::nat) parse-nat])

(examples
 (nat 2)
 => (ctor (pair
           (succ zero)
           (ctor (pair
                  (succ zero)
                  (ctor (pair zero nil))))))

 (t/type-check nat-type (nat 10))
 => true)





(def tree-desc
  (dstruct3 (vls :leaf :node)
            (pair (dk2 :unit)
                  (dprod2 dx2 (dprod2 (dk2 A) dx2))
                  nil)))

(def tree-type (mu2 (dval tree-desc)))


(def tree-nat-desc
  (dstruct3 (vls :leaf :node)
            (pair (dk2 :unit)
                  (dprod2 dx2 (dprod2 (dk2 (dval nat-type)) dx2))
                  nil)))

(def tree-nat-type (mu2 (dval tree-nat-desc)))

(examples
 (t/type-check {'A :type}
               descD
               tree-desc)
 => true

 (t/type-check {'A :type}
               tree-type
               (ctor [zero nil]))
 => true

 (t/type-check {'A :type
                'tl tree-type
                'a (s/parse A)
                'tr tree-type}
               tree-type
               (ctor (pair (succ zero) tl a tr)))
 => true

 (t/type-check descD tree-nat-desc)
 => true

 (t/type-check tree-nat-type (ctor [zero nil]))
 => true

 (t/type-check {'tl tree-nat-type
                'a nat-type
                'tr tree-nat-type}
               tree-nat-type
               (ctor (pair (succ zero) tl a tr)))
 => true)




;; ****************************************************************************
;; *********************************** TREE PARSING ***************************
;; ****************************************************************************

(defterm node ::tree-node)

(def leaf (mk-ctor (a/mk-pair 'zero nil)))

(defn parse-tree-node
  [parse benv term]
  (mk-ctor (a/mk-pair (index 1)
                      (a/mk-pair (parse benv (nth term 1))
                                 (a/mk-pair (parse benv (nth term 2))
                                            (parse benv (nth term 3)))))))

(defn parse-tree-leaf
  [_ _ _]
  leaf)

(s/add-parse-pattern [(u/starts-with ::tree-node) parse-tree-node]
                     [(u/is? 'leaf) parse-tree-leaf])


(examples
 (t/type-check tree-nat-type leaf)
 => true


 (t/type-check tree-nat-type (node leaf (nat 0) leaf))
 => true

 (t/type-check tree-nat-type (node (node leaf (nat 1) leaf)
                                   (nat 4)
                                   leaf))
 => true)
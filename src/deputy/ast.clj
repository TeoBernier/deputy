(ns deputy.ast

  "Abstract Syntax Tree (AST) representation
  and related operations"
  
  (:require
   [clojure.set :as set]
   [clojure.test :refer [is]]
   [deputy.utils :as u :refer [mk-meta]]))

;; ************************************************************************************
;; *********************************** CONSTRUCTORS ***********************************
;; ************************************************************************************


(defn mk-annot
  [type term]
  (mk-meta {:node :annotation
            :type type
            :term term}
           {:deputy true}))

(defn mk-app
  [rator rand]
  (mk-meta {:node :application
            :rator rator
            :rand rand}
           {:deputy true}))

(defn mk-bound
  [name level]
  (mk-meta {:node :bound-var
            :name name
            :level level}
           {:deputy true}))

(defn mk-free
  [name]
  (mk-meta {:node :free-var
            :name name}
           {:deputy true}))

(defn mk-const
  [value]
  value)

(defn mk-lam
  [name body]
  (mk-meta {:node :lambda
            :name name
            :body body}
           {:deputy true}))

(defn mk-pair
  [fst scd]
  (mk-meta {:node :pair
            :first fst
            :second scd}
           {:deputy true}))

(defn mk-sig
  [name fst scd]
  (mk-meta {:node :sig :name name :first fst :second scd}
           {:deputy true}))

(defn mk-proj
  [left-right node]
  (mk-meta {:node :proj :take left-right :pair node}
           {:deputy true}))

(defn mk-pi
  [name dom cod]
  (mk-meta {:node :pi :name name :domain dom :codomain cod}
           {:deputy true}))


;; ************************************************************************************
;; *********************************** METHODS ****************************************
;; ************************************************************************************

(defmulti freevars
  "Returns the set of free variables used in the term `node`."
  (fn [node] (:node node)))

(defmulti unparse-impl
  "Returns a syntax that the parser can read, or a better syntax for debugging, using the default mode or a more specific mode."
  (fn [_ node] (:node node)))

(defn unparse
  ([node] (unparse #{} node))
  ([arg & args] (unparse-impl (into #{arg} (butlast args)) (last args))))

(defmulti locally-closed-impl?
  "Checks if the term node is locally-closed at the index `idx`."
  (fn [_ node] (:node node)))

(defn locally-closed?
  "Checks if the term node is locally-closed at the index `idx`.
   Uses `0` by default."
  ([node] (locally-closed-impl? 0 node))
  ([idx node] (locally-closed-impl? idx node)))

(defmulti normal-form-neutral?
  "Checks if the term `node` is in normal neutral form."
  (fn [node] (:node node)))

(defmulti normal-form-canonical?
  "Checks if the term `node` is in normal canonical form."
  (fn [node] (:node node)))

(defn normal-form?
  "Checks if the term `node` is in beta-normal form."
  [node]
  {:pre [(is (locally-closed? node) "Term must be locally-closed.")]}
  (normal-form-canonical? node))

(defmulti subst-impl
  "Substitutes in term `node` all occurrences of bound variable (i.e. `#{idx}`) by
  (locally-closed) term `expr` at level `idx` (depth of binder)."
  (fn [_ _ node] (:node node)))

(defn subst
  "Substitutes in term `node` all occurrences of bound variable (i.e. `#{idx}`) by
  (locally-closed) term `expr` at level `idx` (depth of binder).
   Uses `0` as default depth."
  ([expr node] (subst expr 0 node))
  ([expr idx node] (subst-impl expr idx node)))

(defmulti bind-impl
  "Bind free variable `name` in term `node` to index `#{idx}`."
  (fn [_ _ node] (:node node)))

(defn bind
  "Bind free variable `name` in term `node` to index `#{idx}`.
   Uses `0` by default."
  ([name term]
   ;;  {:pre  [(is (locally-closed? term)
   ;;              "input term must be locally-closed")]
   ;;   :post [(is (not (contains? (freevars %) name))
   ;;              "input variable cannot be free in the resulting term")
   ;;          (is (locally-closed? 1 %)
   ;;              "resulting term is locally-closed above index 0")]}
   (bind name 0 term))
  ([name idx term] (bind-impl name idx term)))


;; *******************************************************************************
;; *********************************** DEFAULT ***********************************
;; *******************************************************************************


(defmethod freevars :default
  [_]
  #{})

(defmethod unparse-impl :default
  [_ node]
  (throw (ex-info "Cannot unparse the `term`." {:term node})))

(defmethod locally-closed-impl? :default
  [_ _]
  true)

(defmethod normal-form-neutral? :default
  [_]
  false)

(defmethod normal-form-canonical? :default
  [node]
  (normal-form-neutral? node))

(defmethod subst-impl :default
  [_ _ node]
  node)

(defmethod bind-impl :default
  [_ _ node]
  node)



;; **********************************************************************************
;; *********************************** ANNOTATION ***********************************
;; **********************************************************************************


(defmethod unparse-impl :annotation
  [mode node]
  (list 'the
        (unparse-impl mode (:type node))
        (unparse-impl mode (:term node))))

(defmethod freevars :annotation
  [node]
  (set/union (freevars (:type node))
             (freevars (:term node))))

(defmethod locally-closed-impl? :annotation
  [idx node]
  (and (locally-closed-impl? idx (:type node))
       (locally-closed-impl? idx (:term node))))

(defmethod subst-impl :annotation
  [expr idx node]
  (mk-annot (subst-impl expr idx (:type node))
            (subst-impl expr idx (:term node))))

(defmethod bind-impl :annotation
  [name idx node]
  (mk-annot (bind-impl name idx (:type node))
            (bind-impl name idx (:term node))))


;; ***********************************************************************************
;; *********************************** APPLICATION ***********************************
;; ***********************************************************************************


(defmethod unparse-impl :application
  [mode node]
  (list (unparse-impl mode (:rator node))
        (unparse-impl mode (:rand node))))

(defmethod freevars :application
  [node]
  (set/union (freevars (:rator node))
             (freevars (:rand node))))

(defmethod locally-closed-impl? :application
  [idx node]
  (and (locally-closed-impl? idx (:rator node))
       (locally-closed-impl? idx (:rand node))))

(defmethod normal-form-neutral? :application
  [node]
  (and (normal-form-neutral? (:rator node))
       (normal-form-canonical? (:rand node))))

(defmethod subst-impl :application [expr idx node]
  (mk-app (subst-impl expr idx (:rator node))
          (subst-impl expr idx (:rand node))))

(defmethod bind-impl :application
  [name idx node]
  (mk-app (bind-impl name idx (:rator node))
          (bind-impl name idx (:rand node))))


;; *********************************************************************************
;; *********************************** BOUND VAR ***********************************
;; *********************************************************************************


(defmethod unparse-impl :bound-var
  [mode node]
  (if (contains? mode :index)
    #{(:level node)}
    (:name node)))

(defmethod locally-closed-impl? :bound-var
  [idx node]
  (< (:level node) idx))

(defmethod normal-form-neutral? :bound-var
  [_]
  true)

(defmethod subst-impl :bound-var
  [expr idx node]
  (if (= (:level node) idx)
    expr
    node))


;; ********************************************************************************
;; *********************************** FREE VAR ***********************************
;; ********************************************************************************


(defmethod unparse-impl :free-var
  [_ node]
  (:name node))

(defmethod freevars :free-var
  [node]
  #{(:name node)})

(defmethod normal-form-neutral? :free-var
  [_]
  true)

(defmethod bind-impl :free-var
  [name idx node]
  (if (= (:name node) name)
    (mk-bound name idx)
    node))



;; ********************************************************************************
;; *********************************** CONSTANT ***********************************
;; ********************************************************************************


(defmethod unparse-impl nil
  [_ node]
  node)

(defmethod normal-form-canonical? nil
  [_]
  true)


;; ******************************************************************************
;; *********************************** LAMBDA ***********************************
;; ******************************************************************************


(defmethod unparse-impl :lambda
  [mode node]
  (if (contains? mode :index)
    (list 'λ (unparse-impl mode (:body node)))
    (list 'λ [(:name node)] (unparse-impl mode (:body node)))))

(defmethod freevars :lambda
  [node]
  (freevars (:body node)))

(defmethod locally-closed-impl? :lambda
  [idx node]
  (locally-closed-impl? (inc idx) (:body node)))

(defmethod normal-form-canonical? :lambda
  [node]
  (normal-form-canonical? (:body node)))

(defmethod subst-impl :lambda
  [expr idx node]
  (mk-lam (:name node)
          (subst-impl expr (inc idx) (:body node))))

(defmethod bind-impl :lambda
  [name idx node]
  (mk-lam (:name node)
          (bind-impl name (inc idx) (:body node))))


;; ****************************************************************************
;; *********************************** PAIR ***********************************
;; ****************************************************************************


(defmethod unparse-impl :pair
  [mode node]
  [(unparse-impl mode (:first node))
   (unparse-impl mode (:second node))])

(defmethod freevars :pair
  [node]
  (set/union (freevars (:first node))
             (freevars (:second node))))

(defmethod locally-closed-impl? :pair
  [idx node]
  (and (locally-closed-impl? idx (:first node))
       (locally-closed-impl? idx (:second node))))

(defmethod normal-form-canonical? :pair
  [node]
  (and (normal-form-canonical? (:first node))
       (normal-form-canonical? (:second node))))

(defmethod subst-impl :pair
  [expr idx node]
  (mk-pair (subst-impl expr idx (:first node))
           (subst-impl expr idx (:second node))))

(defmethod bind-impl :pair
  [name idx node]
  (mk-pair (bind-impl name idx (:first node))
           (bind-impl name idx (:second node))))



;; ***************************************************************************
;; *********************************** SIG ***********************************
;; ***************************************************************************


(defmethod unparse-impl :sig
  [mode node]
  (if (contains? mode :index)
    (list 'Σ (unparse-impl mode (:first node)) (unparse-impl mode (:second node)))
    (list 'Σ [(:name node) (unparse-impl mode (:first node))] (unparse-impl mode (:second node)))))

(defmethod freevars :sig
  [node]
  (set/union (freevars (:first node))
             (freevars (:second node))))

(defmethod locally-closed-impl? :sig
  [idx node]
  (and (locally-closed-impl? idx (:first node))
       (locally-closed-impl? (inc idx) (:second node))))

(defmethod normal-form-canonical? :sig
  [node]
  (and (normal-form-canonical? (:first node))
       (normal-form-canonical? (:second node))))

(defmethod subst-impl :sig [expr idx node]
  (mk-sig
   (:name node)
   (subst-impl expr idx (:first node))
   (subst-impl expr (inc idx) (:second node))))

(defmethod bind-impl :sig
  [name idx node]
  (mk-sig
   (:name node)
   (bind-impl name idx (:first node))
   (bind-impl name (inc idx) (:second node))))

; ****************************************************************************
;; *********************************** PROJ ***********************************
;; ****************************************************************************



(defmethod unparse-impl :proj
  [mode node]
  (list
   (case (:take node)
     :left 'π1
     :right 'π2)
   (unparse-impl mode (:pair node))))

(defmethod freevars :proj
  [node]
  (freevars (:pair node)))

(defmethod locally-closed-impl? :proj
  [idx node]
  (locally-closed-impl? idx (:pair node)))

(defmethod normal-form-neutral? :proj
  [node]
  (normal-form-neutral? (:pair node)))

(defmethod subst-impl :proj
  [expr idx node]
  (mk-proj (:take node)
           (subst-impl expr idx (:pair node))))

(defmethod bind-impl :proj
  [name idx node]
  (mk-proj (:take node) (bind-impl name idx (:pair node))))



;; **************************************************************************
;; *********************************** PI ***********************************
;; **************************************************************************


(defmethod unparse-impl :pi
  [mode node]
  (list 'Π
        (if (contains? mode :index)
          (unparse-impl mode (:domain node))
          [(:name node) (unparse-impl mode (:domain node))])
        (unparse-impl mode (:codomain node))))

(defmethod freevars :pi
  [node]
  (set/union (freevars (:domain node))
             (freevars (:codomain node))))

(defmethod locally-closed-impl? :pi
  [idx node]
  (and (locally-closed-impl? idx (:domain node))
       (locally-closed-impl? (inc idx) (:codomain node))))

(defmethod normal-form-canonical? :pi
  [node]
  (and (normal-form-canonical? (:domain node))
       (normal-form-canonical? (:codomain node))))

(defmethod subst-impl :pi
  [expr idx node]
  (mk-pi (:name node)
         (subst-impl expr idx (:domain node))
         (subst-impl expr (inc idx) (:codomain node))))

(defmethod bind-impl :pi
  [name idx node]
  (mk-pi (:name node)
         (bind-impl name idx (:domain node))
         (bind-impl name (inc idx) (:codomain node))))



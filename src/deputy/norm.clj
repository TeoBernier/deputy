(ns deputy.norm
  (:require
   [clojure.set :as set]
   [deputy.syntax :as s :refer [parse fun]]
   [deputy.ast :as a]
   [deputy.utils :as u :refer [ok>]]))


;; temporary assert macro
(defmacro ensure [cond & body]
  `(do (assert ~cond)
       ~@body))

;; ************************************************************************************
;; *********************************** ENV ********************************************
;; ************************************************************************************


(defn env-push
  "Push value `v` in evaluation `env`ironment."
  [env v]
  (conj env v))

(defn env-fetch
  "Fetch `n`-th value in `env`ironment."
  [env n]
  (nth env (- (count env) (inc n))))

;; ************************************************************************************
;; *********************************** METHODS ********************************************
;; ************************************************************************************


(defmulti evaluate-impl
  "Evaluate term `node` in environment `env` by piggy-backing on Clojure's β-reduction."
  (fn [_ node] (:node node)))

(defn evaluate
  "Evaluate term `node` in environment `env` by piggy-backing on Clojure's β-reduction.
   Uses empty environment by default."
  ([term]
   {:pre [(a/locally-closed? term)]}
   (evaluate-impl [] term)))

(defmulti alpha-equiv
  "Checks if `node1` and `node2` are α-equivalent. 
   Expressions have to be evaluated before the check !"
  (fn [node1 node2] (and (= (:node node1) (:node node2)) (:node node1))))

(defn beta-equiv
  [node1 node2]
  (alpha-equiv (evaluate node1) (evaluate node2)))



;; ***********************************************************************************
;; *********************************** EVALUATE **************************************
;; ***********************************************************************************

;;; XXX : this, or let the multi fail? (what's idiomatic?)
(defmethod evaluate-impl :default
  [_ node]
  (throw (ex-info "Cannot normalize term `node`." {:term (a/unparse node)})))

(defmethod evaluate-impl :annotation
  [env node]
  (evaluate-impl env (:term node)))

(defmethod evaluate-impl :application
  [env node]
  (let [rator (evaluate-impl env (:rator node))
        rand  (evaluate-impl env (:rand node))]
   (if (= (u/node-type rator) :lambda)
     (evaluate-impl (env-push env rand) (:body rator))
     (ensure (a/normal-form-neutral? node)
             (a/mk-app rator rand)))))

(defmethod evaluate-impl :bound-var
  [env node]
  (env-fetch env (:level node)))

(defmethod evaluate-impl :free-var
  [_ node]
  node)

(defmethod evaluate-impl nil
  [_ node]
  node)

(defmethod evaluate-impl :lambda
  [env node]
  (let [name (s/gen-free-name (:name node) (a/freevars node))
        body-norm (evaluate-impl (env-push env (a/mk-free name)) (:body node))]
    (a/mk-lam name (a/bind name body-norm))))

(defmethod evaluate-impl :pair
  [env node]
  (let [first (evaluate-impl env (:first node))
        second (evaluate-impl env (:second node))]
   (a/mk-pair first second)))

(defmethod evaluate-impl :sig
  [env node]
  (let [first (evaluate-impl env (:first node))
        name (s/gen-free-name (:name node) (a/freevars (:second node)))
        second (evaluate-impl (env-push env (a/mk-free name)) (:second node))]
    (a/mk-sig name first (a/bind name second))))

(defmethod evaluate-impl :proj
  [env node]
  (let [pair (evaluate-impl env (:pair node))]
    (case (u/node-type pair)
      :pair (case (:take node)
              :left (evaluate-impl env (:first pair))
              :right (evaluate-impl env (:second pair)))
      (ensure (a/normal-form-neutral? pair)
              (a/mk-proj (:take node) pair)))))

(defmethod evaluate-impl :pi
  [env node]
  (let [domain (evaluate-impl env (:domain node))
        name (s/gen-free-name (:name node) (a/freevars (:codomain node)))
        codomain (evaluate-impl (env-push env (a/mk-free name)) (:codomain node))]
   (a/mk-pi name domain (a/bind name codomain))))


;; ***********************************************************************************
;; *********************************** ALPHA-EQUIV ***********************************
;; ***********************************************************************************


(defmethod alpha-equiv :default [node1 node2] (= node1 node2))
(defmethod alpha-equiv :false [_ _] false)

(defmethod alpha-equiv :application
  [node1 node2]
  (and (alpha-equiv (:rator node1) (:rator node2))
       (alpha-equiv (:rand node1) (:rand node2))))

(defmethod alpha-equiv :bound-var
  [node1 node2]
  (= (:level node1) (:level node2)))

(defmethod alpha-equiv :free-var
  [node1 node2]
  (= (:name node1) (:name node2)))

(defmethod alpha-equiv :lambda
  [node1 node2]
  (alpha-equiv (:body node1) (:body node2)))

(defmethod alpha-equiv :pair
  [node1 node2]
  (and (alpha-equiv (:first node1) (:first node2))
       (alpha-equiv (:second node1) (:second node2))))

(defmethod alpha-equiv :sig
  [node1 node2]
  (and (alpha-equiv (:first node1) (:first node2))
       (alpha-equiv (:second node1) (:second node2))))

(defmethod alpha-equiv :proj
  [node1 node2]
  (and (alpha-equiv (:take node1) (:take node2))
       (alpha-equiv (:pair node1) (:pair node2))))

(defmethod alpha-equiv :pi
  [node1 node2]
  (and (alpha-equiv (:domain node1) (:domain node2))
       (alpha-equiv (:codomain node1) (:codomain node2))))

